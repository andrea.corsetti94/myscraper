package controller;

import http.HTTPManager;
import main.Logger;
import model.Algorithm;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

class AlgorithmRetriever {

    private static final String AlGORITHMS_URL = "https://hintwise.com/tipsters";
    private static final String AJAX_QUERY_FOR_PAGES = "?page=";
    private static final int MAX_NUM_ALGO_FOR_PAGE = 20;


    public static List<Algorithm> retrieveAlgorithms(int limitPages) {
        return extractAlgorithms(1, limitPages);
    }

    public static List<Algorithm> retrieveAlgorithms(int begin, int limitPages) {
        return extractAlgorithms(begin, limitPages);
    }

    private static List<Algorithm> extractAlgorithms(int begin, int end) {
        List<Algorithm> algorithms = new ArrayList<>();

        //loop on pages
        for (int i = begin; i <= end; i++) {

            String url = AlGORITHMS_URL + AJAX_QUERY_FOR_PAGES + i;

            //loop on rows/algorithms in one page
            for (int j = 1; j <= MAX_NUM_ALGO_FOR_PAGE; j++) {
                String htlmSelector = "#blog-index > table > tbody > tr:nth-child(" + j + ")";

                Elements select = HTTPManager.getHTMLElements(url, htlmSelector, 3);

                if (select == null) continue;

                Element rowTables = select.get(0);

                if (rowTables.getAllElements().size() == 0) {
                    break; //go on next page
                }

                Elements rowTableTd = rowTables.getElementsByTag("td");
                String algoURL = rowTableTd.get(0).getElementsByTag("a").attr("abs:href");

                Algorithm algorithm = new Algorithm(algoURL);
                algorithms.add(algorithm);
            }

            Logger.getLogger(AlgorithmRetriever.class).debug("Algorithms retrieved for page: " + i);

        }
        return algorithms;
    }
}
