package controller;

import file.ExcelManager;
import file.FileManager;
import main.Logger;
import model.Algorithm;
import model.AlgorithmEvaluation;
import model.EvaluationChart;
import model.Prediction;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

class AlgorithmEvaluator {


    public static void evaluateAlgorithms(List<Algorithm> algorithms) {

        String timestamp = new SimpleDateFormat("dd-MM-yyyy_HH-mm").format(new Timestamp(System.currentTimeMillis()));

        String filename = "Evaluation_" + timestamp;

        StringBuilder content = new StringBuilder("Evaluation:\n");

        List<AlgorithmEvaluation> evaluations = new ArrayList<>();
        for (Algorithm algo : algorithms) {
            AlgorithmEvaluation evaluation = evaluateAlgorithm(algo.getId());
            if (evaluation != null) {
                Logger.getLogger(Updater.class).debug("Evaluated Algo: " + algo.getId() + " : " + evaluation.toString());
                evaluations.add(evaluation);
            }
        }

        Collections.sort(evaluations);

        for (AlgorithmEvaluation evaluation : evaluations) {
            content.append(evaluation.toString()).append(System.lineSeparator()).append("-------------------------------").append(System.lineSeparator());
        }

        FileManager.writeTxt(content.toString(), filename);


        //update chart evaluations
        EvaluationChart chart = new EvaluationChart(timestamp, evaluations);
        Updater.updateEvaluationChart(chart);
    }

    public static AlgorithmEvaluation evaluateAlgorithm(String algoId) {
        Map<String, Prediction> predictionsMap = ExcelManager.readPredictions(algoId);

        List<Prediction> predictions = new ArrayList<>(predictionsMap.values());
        if (predictions.isEmpty()) {
            System.out.println("No predictions found for Algorithm: " + algoId);
            return null;
        }

        int profit = 0;
        int wins = 0;
        for (Prediction pred : predictions) {
            int predProfit = pred.getPredictionResult().getProfit();
            profit += predProfit;
            if (predProfit > 0) {
                wins++;
            }

        }

        return new AlgorithmEvaluation(algoId, wins, predictions.size(), profit);
    }


    public static int compareAlgorithms(Algorithm algo1, Algorithm algo2) {
        AlgorithmEvaluation evaluationAlgo1 = evaluateAlgorithm(algo1.getId());
        AlgorithmEvaluation evaluationAlgo2 = evaluateAlgorithm(algo2.getId());

        if (evaluationAlgo1 == null || evaluationAlgo2 == null) {
            System.out.println("Impossible to evaluate null Algorithm Evaluation");
            return 0;
        }

        return evaluationAlgo1.compareTo(evaluationAlgo2);
    }
}
