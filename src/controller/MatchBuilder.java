package controller;

import main.Logger;
import model.Match;

public class MatchBuilder {

    public static Match buildMatch(String date, String score, String sport, String matchTeams) {
        return new Match(date, score, sport, normalizeTeams(matchTeams));
    }

    private static String normalizeTeams(String teams) {
        try {
            String teamA = teams.split("-")[0].trim();
            String teamB = teams.split("-")[1].trim();
            return teamA + " vs " + teamB;
        } catch (Exception e) {
            Logger.getLogger(MatchBuilder.class).error("Error extracting prediction. Teams value not formatted " +
                    "properly. It has to be formatted using divider: '" + "-" + "' to divide the teams. Change this attribute if needed.\n" +
                    "Teams: " + teams, e);
        }
        return teams;
    }
}
