package controller;

import configuration.Configs;
import file.ExcelManager;
import main.Logger;
import model.Algorithm;
import model.AlgorithmEvaluation;
import model.EvaluationChart;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.List;


public class Updater {

    public static void updateAlgorithmPredictions(int limitAlgorithmPages) {
        List<Algorithm> algorithms = AlgorithmRetriever.retrieveAlgorithms(limitAlgorithmPages);
        updatePredictions(algorithms);
    }

    public static void updateAlgorithmPredictions(int begin, int end) {
        List<Algorithm> algorithms = AlgorithmRetriever.retrieveAlgorithms(begin,end);
        updatePredictions(algorithms);
    }

    private static void updatePredictions(List<Algorithm> algorithms){
        algorithms.forEach(algo -> {
            Logger.getLogger(Updater.class).debug("Analyzing Algorithm with ID: " + algo.getId());
            try {
                PredictionRetriever.updatePredictions(algo);
            } catch (Exception e) {
                Logger.getLogger(Updater.class).error("Exception raised while updating algorithm: " + algo.getId(), e);
            }
        });

        Logger.getLogger(Updater.class).debug("Update Completed.");
        AlgorithmEvaluator.evaluateAlgorithms(algorithms);
        Logger.getLogger(Updater.class).debug("Evaluation Completed and stored in TXT.");
    }

    public static void updateEvaluation(String algoId) {

        AlgorithmEvaluation evaluation = AlgorithmEvaluator.evaluateAlgorithm(algoId);

        String filename = Configs.getExcelAlgorithmsEvaluationDirectory() + algoId + "_Evaluation.xlsx";
        File file = new File(filename);

        Workbook wb;
        Sheet sheet;
        int num;
        if (file.exists()) {
            Logger.getLogger(Updater.class).debug("Evaluation file for Algorithm with ID: " + algoId + " already exists.");

            InputStream inp;
            try {
                inp = new FileInputStream(Configs.getExcelAlgorithmsEvaluationDirectory() + algoId + "_Evaluation.xlsx");
                wb = WorkbookFactory.create(inp);
                sheet = wb.getSheetAt(0);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        } else {

            Logger.getLogger(Updater.class).debug("Evaluation file for Algorithm with ID: " + algoId + " DOES NOT YET exists.");

            wb = new XSSFWorkbook();
            sheet = wb.createSheet("Evaluation");

            num = sheet.getLastRowNum();
            Row row = sheet.createRow(num);

            row.createCell(0).setCellValue("TIMESTAMP");
            row.createCell(1).setCellValue("ROI");
            row.createCell(2).setCellValue("TOTAL PREDICTIONS");
            row.createCell(3).setCellValue("WON");
            row.createCell(4).setCellValue("LOST");
            row.createCell(5).setCellValue("PROFIT");
        }

        int lastRow = sheet.getLastRowNum();
        ExcelManager.shiftRows(sheet, 1, lastRow, 1);
        Row row = sheet.createRow(1);

        row.createCell(0).setCellValue(evaluation.getTimestamp());
        row.createCell(1).setCellValue(String.valueOf(evaluation.getROI()));
        row.createCell(2).setCellValue(String.valueOf(evaluation.getPredictionSize()));
        row.createCell(3).setCellValue(String.valueOf(evaluation.getWins()));
        row.createCell(4).setCellValue(String.valueOf(evaluation.getLosses()));
        row.createCell(5).setCellValue(String.valueOf(evaluation.getProfit()));

        Logger.getLogger(Updater.class).debug("Evaluation for Algorithm with ID: " + algoId + " : " + evaluation.toString());

        for (int i = 0; i < 6; i++) {
            sheet.autoSizeColumn(i);
        }
        // Write the output to a file
        try {
            FileOutputStream fileOut = new FileOutputStream(Configs.getExcelAlgorithmsEvaluationDirectory() + algoId + "_Evaluation.xlsx");
            wb.write(fileOut);
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                wb.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static void updateEvaluationChart(EvaluationChart chart) {
        Workbook wb = null;
        try {
            InputStream inp = new FileInputStream(Configs.getEvaluationChartFile());

            wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);

            int lastRow = sheet.getLastRowNum();
            ExcelManager.shiftRows(sheet, 1, lastRow, 1);

            Row newRow = sheet.createRow(1);

            newRow.createCell(0).setCellValue(chart.getTimestamp());

            for (int i = 0; i < chart.getEvaluations().size(); i++) {
                AlgorithmEvaluation evaluation = chart.getEvaluations().get(i);
                newRow.createCell(i + 1).setCellValue(evaluation.getAlgorithmId() + " - ROI: " + evaluation.getROI());
                sheet.autoSizeColumn(i);
            }

            // Write the output to a file
            FileOutputStream fileOut = new FileOutputStream(Configs.getEvaluationChartFile());
            wb.write(fileOut);
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                wb.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
