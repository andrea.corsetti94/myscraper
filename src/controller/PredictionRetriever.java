package controller;

import file.ExcelManager;
import file.HashUtilities;
import http.HTTPManager;
import main.Logger;
import model.*;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class PredictionRetriever {

    private static final int MAX_NUM_PAGES = 3;
    private static final int MAX_NUM_ROWS = 50;
    //private static final String AJAX_QUERY_FOR_PAGES = "?ajax=history-predictions&page=";
    private static final String AJAX_QUERY_FOR_PAGES = "?page=";

    public static void updatePredictions(Algorithm algorithm) {
        List<Prediction> newPredictions = PredictionRetriever.retrieveOnlyNewPredictions(algorithm);
        Logger.getLogger(PredictionRetriever.class)
                .debug("New predictions retrieved for algorithm: " + algorithm.getId() + " : " + newPredictions.size());

        newPredictions.forEach(pred -> Logger.getLogger(PredictionRetriever.class).debug("New prediction: " + pred.toString()));

        if (newPredictions.size() > 0) {
            ExcelManager.writePredictions(newPredictions, algorithm.getId());
        }
    }

    private static List<Prediction> retrieveOnlyNewPredictions(Algorithm algorithm) {
        //1. serve una previsione limite oltre la quale non controllare oltre.
        //questa previsione sara' l'ultima previsione nel file.

        Prediction limitPrediction = ExcelManager.retrieveLastPrediction(algorithm);

        return retrievePredictionsWithLimit(algorithm, limitPrediction);
    }

    private static List<Prediction> retrievePredictionsWithLimit(Algorithm algorithm, Prediction limitPrediction) {

        String hashedLimitPrediction = "";
        if (!(limitPrediction instanceof EmptyPrediction)) {
            hashedLimitPrediction = HashUtilities.hashPrediction(limitPrediction);
            Logger.getLogger(PredictionRetriever.class).debug("Limit prediction for Algorithm " + algorithm.getId() + ": " + limitPrediction.toString());
        }
        else{
            Logger.getLogger(PredictionRetriever.class).debug("NO Limit prediction for Algorithm " + algorithm.getId());
        }

        List<Prediction> predictions = new ArrayList<>();

        String algoUrl = algorithm.getUrl();

        String pageUrl;
        for (int i = 1; i <= MAX_NUM_PAGES; i++) {
            pageUrl = algoUrl + AJAX_QUERY_FOR_PAGES + i;

            for (int j = 1; j <= MAX_NUM_ROWS; j++) {
                String htmlSelector = "#history-predictions > table > tbody > tr:nth-child(" +
                        j + ")";

                Prediction currentPrediction = PredictionRetriever.extractPrediction(pageUrl, htmlSelector, algorithm.getId());
                if (currentPrediction.isEmpty()) {
                    break;
                    //it means we reached the maximum number of predictions for this page
                }

                String hashedCurrentPrediction = HashUtilities.hashPrediction(currentPrediction);

                if (hashedLimitPrediction.equals(hashedCurrentPrediction)) {
                    return predictions;
                }

                if (predictions.contains(currentPrediction)) {
                    //System.out.println("WARN: PREDICTION ALREADY INSERTED: " + currentPrediction);
                } else {
                    predictions.add(currentPrediction);
                }
            }
        }

        return predictions;
    }

    private static Prediction extractPrediction(String algorithmPageUrl, String htlmSelector, String algorithmId) {

        Elements select = HTTPManager.getHTMLElements(algorithmPageUrl, htlmSelector, 3);

        if (select == null || select.size() == 0) {
            return new EmptyPrediction();
        }

        Element rowTables = select.get(0);

        if (rowTables.getAllElements().size() == 0) {
            return new EmptyPrediction();
        }

        Element date;
        Element sport;
        Element teams;
        Element predictionResult = null;
        Element score;
        Element outcome;
        Match match = null;
        PredictionResult result = null;

        try {
            Elements rowTableTd = rowTables.getElementsByTag("td");

            date = rowTableTd.get(0);
            sport = rowTableTd.get(1);
            teams = rowTableTd.get(3).getElementsByAttribute("href").first().getElementsByTag("h4").first();
            predictionResult = rowTableTd.get(4);
            score = rowTableTd.get(5);
            outcome = PredictionResultBuilder.buildOutcomeElement(rowTableTd.get(6));

            match = MatchBuilder.buildMatch(date.ownText(), score.ownText(), sport.ownText(), teams.ownText());
            result = PredictionResultBuilder.buildResult(outcome.ownText());

			/*
			Logger.getLogger(PredictionRetriever.class).debug("Prediction values:\nDate: " + date.ownText() + "\nSport: " + sport.ownText()
					+ "\nTeams: " + teams.ownText() + "\nPrediction: "+ predictionResult.ownText()+"\nScore: "+ score.ownText()
					+ "\nOutcome: " + outcome.ownText());
			*/
        } catch (NullPointerException e) {
            //extraction has failed
            Logger.getLogger(PredictionRetriever.class).error("Extraction of page: " + algorithmPageUrl +
                    " has failed. HTML Selector: " + htlmSelector + "\nHTML: " + select.outerHtml(), e);
        }

        return new Prediction(match, predictionResult.ownText(), result, algorithmId);

    }
}
