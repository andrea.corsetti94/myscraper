package controller;

import model.PredictionResult;
import model.PredictionResult.Outcome;
import org.jsoup.nodes.Element;

class PredictionResultBuilder {

    public static PredictionResult buildResult(String resultField) {
        String normalized = resultField.replaceAll("[^0-9]", "").trim();

        if (resultField.trim().startsWith("W")) {
            return new PredictionResult(Integer.parseInt(normalized), Outcome.WIN);
        } else if (resultField.trim().startsWith("L")) {
            return new PredictionResult(Integer.parseInt(normalized) * -1, Outcome.LOST);
        } else {
            return null;
        }
    }

    public static Element buildOutcomeElement(Element rowElement) {
        if (rowElement.getElementsByClass("win-color").size() > 0) {
            return rowElement.getElementsByClass("win-color").first();
        } else if (rowElement.getElementsByClass("lose-color").size() > 0) {
            return rowElement.getElementsByClass("lose-color").first();
        }
        return null;
    }
}
