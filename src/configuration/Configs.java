package configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configs {

    private static String excelAlgorithmsPredictionDirectory;
    private static String excelAlgorithmsEvaluationDirectory;
    private static String txtEvaluationDirectory;
    private static String evaluationChartFile;

    static {
        init();
    }

    private static void init() {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            input = Configs.class.getResourceAsStream("/configuration.properties");

            // load a properties file
            prop.load(input);

            // get the property value and print it out

            setExcelAlgorithmsEvaluationDirectory(prop.getProperty("AlgorithmsEvaluationDirectory"));
            setExcelAlgorithmsPredictionDirectory(prop.getProperty("AlgorithmsPredictionDirectory"));
            setTxtEvaluationDirectory(prop.getProperty("TxtEvaluationDirectory"));
            setEvaluationChartFile(prop.getProperty("EvaluationChartPath"));

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String getExcelAlgorithmsPredictionDirectory() {
        return excelAlgorithmsPredictionDirectory;
    }

    private static void setExcelAlgorithmsPredictionDirectory(String excelAlgorithmsPredictionDirectory) {
        Configs.excelAlgorithmsPredictionDirectory = excelAlgorithmsPredictionDirectory;
    }

    public static String getExcelAlgorithmsEvaluationDirectory() {
        return excelAlgorithmsEvaluationDirectory;
    }

    private static void setExcelAlgorithmsEvaluationDirectory(String excelAlgorithmsEvaluationDirectory) {
        Configs.excelAlgorithmsEvaluationDirectory = excelAlgorithmsEvaluationDirectory;
    }

    public static String getTxtEvaluationDirectory() {
        return txtEvaluationDirectory;
    }

    private static void setTxtEvaluationDirectory(String txtEvaluationDirectory) {
        Configs.txtEvaluationDirectory = txtEvaluationDirectory;
    }

    private static void setEvaluationChartFile(String evaluationChartFile) {
        Configs.evaluationChartFile = evaluationChartFile;
    }

    public static String getEvaluationChartFile() {
        return Configs.evaluationChartFile;
    }
}
