package model;

import java.util.List;

public class EvaluationChart {

    private final String timestamp;

    private final List<AlgorithmEvaluation> evaluations;

    public String getTimestamp() {
        return timestamp;
    }

    public EvaluationChart(String timestamp, List<AlgorithmEvaluation> evaluations) {
        this.timestamp = timestamp;
        this.evaluations = evaluations;
    }

    public List<AlgorithmEvaluation> getEvaluations() {
        return evaluations;
    }
}
