package model;

public class Algorithm {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Algorithm(String url) {
        super();
        this.url = url;
    }

    public String getId() {
        return this.getUrl().substring(this.getUrl().lastIndexOf("/") + 1);
    }

}
