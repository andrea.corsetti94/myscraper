package model;

public class EmptyPrediction extends Prediction {

    public EmptyPrediction() {
        super(null, null, null, null);
    }

    @Override
    public boolean isEmpty() {
        return true;
    }

}
