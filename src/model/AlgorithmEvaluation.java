package model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class AlgorithmEvaluation implements Comparable<AlgorithmEvaluation> {

    private String algorithmId;

    private final String timestamp;

    private double ROI;

    private int wins;

    private int losses;

    private int predictionSize;

    private int profit;

    public AlgorithmEvaluation(String algorithmId, int wins, int predictionSize, int profit) {
        super();

        assert (predictionSize != 0);

        this.algorithmId = algorithmId;
        this.wins = wins;
        this.predictionSize = predictionSize;
        this.profit = profit;

        this.ROI = this.profit / this.predictionSize;
        this.losses = this.predictionSize - this.wins;

        this.timestamp = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(new Timestamp(System.currentTimeMillis()));
    }

    public String getAlgorithmId() {
        return algorithmId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public double getROI() {
        return ROI;
    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    public int getPredictionSize() {
        return predictionSize;
    }

    public int getProfit() {
        return profit;
    }

    @Override
    public String toString() {
        return "AlgorithmEvaluation [algorithm=" + algorithmId + ", timestamp=" + timestamp + ", ROI=" + ROI + ", wins="
                + wins + ", losses=" + losses + ", predictionSize=" + predictionSize + ", profit=" + profit + "]";
    }

    @Override
    public int compareTo(AlgorithmEvaluation o) {
        if (o.getROI() > this.getROI()) return 1;
        else if (o.getROI() < this.getROI()) return -1;

        return 0;
    }


}
