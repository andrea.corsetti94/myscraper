package model;

public class Prediction {

    private Match match;

    private String prediction;

    private PredictionResult predictionResult;

    private String algorithmId;

    public String getAlgorithmId() {
        return algorithmId;
    }

    public void setAlgorithmId(String algorithmId) {
        this.algorithmId = algorithmId;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public String getPrediction() {
        return prediction;
    }

    public void setPrediction(String prediction) {
        this.prediction = prediction;
    }

    public PredictionResult getPredictionResult() {
        return predictionResult;
    }

    public void setPredictionResult(PredictionResult predictionResult) {
        this.predictionResult = predictionResult;
    }

    public Prediction(Match match, String prediction, PredictionResult predictionResult, String algorithmId) {
        super();
        this.match = match;
        this.prediction = prediction;
        this.predictionResult = predictionResult;
        this.algorithmId = algorithmId;
    }


    @Override
    public String toString() {

        return this.getMatch().toString() +
                " - Prediction: " + this.getPrediction() +
                this.getPredictionResult().toString();

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((match == null) ? 0 : match.hashCode());
        result = prime * result + ((prediction == null) ? 0 : prediction.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Prediction other = (Prediction) obj;
        if (match == null) {
            if (other.match != null)
                return false;
        } else if (!match.equals(other.match))
            return false;
        if (prediction == null) {
            return other.prediction == null;
        } else return prediction.equals(other.prediction);
    }

    public boolean isEmpty() {
        return false;
    }

}
