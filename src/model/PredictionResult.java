package model;

public class PredictionResult {

    public enum Outcome {
        WIN, LOST;

        public static Outcome fromString(String input) {
            if (input.trim().startsWith("W")) {
                return Outcome.WIN;
            } else if (input.trim().startsWith("L")) {
                return Outcome.LOST;
            }
            return null;
        }

    }

    private int profit;

    private Outcome outcome;

    public PredictionResult(int profit, Outcome outcome) {
        super();
        this.profit = profit;
        this.outcome = outcome;
    }

    public int getProfit() {
        return profit;
    }

    public void setProfit(int profit) {
        this.profit = profit;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public void setOutcome(Outcome outcome) {
        this.outcome = outcome;
    }

    @Override
    public String toString() {
        return " - Outcome: " + this.getOutcome().toString() + " - Profit: " + this.getProfit();
    }


}
