package file;

import configuration.Configs;
import main.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileManager {

    public static void writeTxt(String content, String filename) {

        String path = Configs.getTxtEvaluationDirectory()
                + filename + ".txt";

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path, true))) {

            bw.write(content);

        } catch (IOException e) {
            Logger.getLogger(FileManager.class).error("Error writing file: " + path + " IGNORE.", e);
        }
    }

}