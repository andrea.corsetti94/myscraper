package file;

import configuration.Configs;
import controller.Updater;
import main.Logger;
import model.*;
import model.PredictionResult.Outcome;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.*;

@SuppressWarnings({"ALL", "JavaDoc"})
public class ExcelManager {

    /**
     * check if excel file relative to the algorithm identified by algoId exists. If it does, append the content
     * to the file, if it doesn't create the file and write the predictions into it.
     *
     * @param predictions
     * @param algoId
     */
    public static void writePredictions(List<Prediction> predictions, String algoId) {
        File file = new File(ExcelManager.buildFileName(algoId));

        //reverse predictions:
        Collections.reverse(predictions);

        if (file.exists()) {
            Logger.getLogger(ExcelManager.class).debug("Prediction file for algo: " + algoId + " already exists.");
            ExcelManager.appendPredictions(predictions, algoId);
        } else {
            Logger.getLogger(ExcelManager.class).debug("Prediction file for algo: " + algoId + " DOES NOT exists YET.");
            ExcelManager.createAlgorithmPredictionFile(predictions, algoId);
        }

        Updater.updateEvaluation(algoId);
    }

    private static void createAlgorithmPredictionFile(List<Prediction> predictions, String algoId) {

        //Create blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();

        //Create a blank sheet
        XSSFSheet spreadsheet = workbook.createSheet(algoId);

        XSSFRow row = spreadsheet.createRow(0);

        row.createCell(0).setCellValue("HASH");
        row.createCell(1).setCellValue("DATE");
        row.createCell(2).setCellValue("SPORT");
        row.createCell(3).setCellValue("TEAMS");
        row.createCell(4).setCellValue("SCORE");
        row.createCell(5).setCellValue("PREDICTION");
        row.createCell(6).setCellValue("OUTCOME");
        row.createCell(7).setCellValue("PROFIT");

        predictions.forEach(pred -> {
            int num = spreadsheet.getLastRowNum();
            XSSFRow newxtRow = spreadsheet.createRow(++num);

            Match match = pred.getMatch();
            PredictionResult result = pred.getPredictionResult();

            newxtRow.createCell(0).setCellValue(HashUtilities.hashPrediction(pred));
            newxtRow.createCell(1).setCellValue(match.getDate());
            newxtRow.createCell(2).setCellValue(match.getSport());
            newxtRow.createCell(3).setCellValue(match.getMatch());
            newxtRow.createCell(4).setCellValue(match.getScore());
            newxtRow.createCell(5).setCellValue(pred.getPrediction());
            newxtRow.createCell(6).setCellValue(result.getOutcome().toString());
            newxtRow.createCell(7).setCellValue(String.valueOf(result.getProfit()));

            Logger.getLogger(ExcelManager.class).debug("Prediction created for algo: " + algoId + " : " + pred.toString());

        });

        //adjust column width except for hash, that is hidden
        spreadsheet.setColumnHidden(0, true);
        for (int i = 1; i <= 7; i++) {
            spreadsheet.autoSizeColumn(i);
        }
        //Write the workbook in file system

        try {
            FileOutputStream out = new FileOutputStream(
                    new File(ExcelManager.buildFileName(algoId)));

            workbook.write(out);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private static void appendPredictions(List<Prediction> predictions, String algoId) {

        Workbook wb = null;
        try {
            InputStream inp = new FileInputStream(ExcelManager.buildFileName(algoId));

            wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);

            predictions.forEach(pred -> {
                int num = sheet.getLastRowNum();
                Row row = sheet.createRow(++num);

                Match match = pred.getMatch();
                PredictionResult result = pred.getPredictionResult();

                row.createCell(0).setCellValue(HashUtilities.hashPrediction(pred));
                row.createCell(1).setCellValue(match.getDate());
                row.createCell(2).setCellValue(match.getSport());
                row.createCell(3).setCellValue(match.getMatch());
                row.createCell(4).setCellValue(match.getScore());
                row.createCell(5).setCellValue(pred.getPrediction());
                row.createCell(6).setCellValue(result.getOutcome().toString());
                row.createCell(7).setCellValue(String.valueOf(result.getProfit()));

                Logger.getLogger(ExcelManager.class).debug("Prediction appended for algo: " + algoId + " : " + pred.toString());

            });

            // Write the output to a file
            FileOutputStream fileOut = new FileOutputStream(ExcelManager.buildFileName(algoId));
            wb.write(fileOut);
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                wb.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Map<String, Prediction> readPredictions(String algoId) {

        Map<String, Prediction> predictions = new HashMap<>();

        XSSFWorkbook workbook = null;

        try {
            FileInputStream file = new FileInputStream(new File(ExcelManager.buildFileName(algoId)));

            workbook = new XSSFWorkbook(file);
            XSSFSheet sheet = workbook.getSheetAt(0);

            // Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();

            int rowCounter = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();

                //first line is header, skip it
                if (rowCounter == 0) {
                    rowCounter++;
                    continue;
                }

                String hashPrediction = row.getCell(0).getStringCellValue();
                String datePrediction = row.getCell(1).getStringCellValue();
                String sportPrediction = row.getCell(2).getStringCellValue();
                String teamsPrediction = row.getCell(3).getStringCellValue();
                String scorePrediction = row.getCell(4).getStringCellValue();
                String predictionValue = row.getCell(5).getStringCellValue();
                String outcomePrediction = row.getCell(6).getStringCellValue();
                String profitPrediction = row.getCell(7).getStringCellValue();

                Match match = new Match(datePrediction, scorePrediction, sportPrediction, teamsPrediction);
                PredictionResult result = new PredictionResult(Integer.parseInt(profitPrediction), Outcome.fromString(outcomePrediction));

                Prediction prediction = new Prediction(match, predictionValue, result, algoId);
                predictions.put(hashPrediction, prediction);

                rowCounter++;
            }
            file.close();
        } catch (FileNotFoundException ex) {
            predictions = new HashMap<>();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (workbook != null) workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return predictions;
    }

    public static Prediction retrieveLastPrediction(Algorithm algorithm) {
        XSSFWorkbook workbook = null;

        try {
            FileInputStream file = new FileInputStream(new File(ExcelManager.buildFileName(algorithm.getId())));

            workbook = new XSSFWorkbook(file);
            XSSFSheet sheet = workbook.getSheetAt(0);

            int lastRowIndex = sheet.getLastRowNum();

            XSSFRow lastRow = sheet.getRow(lastRowIndex);

            String datePrediction = lastRow.getCell(1).getStringCellValue();
            String sportPrediction = lastRow.getCell(2).getStringCellValue();
            String teamsPrediction = lastRow.getCell(3).getStringCellValue();
            String scorePrediction = lastRow.getCell(4).getStringCellValue();
            String predictionValue = lastRow.getCell(5).getStringCellValue();
            String outcomePrediction = lastRow.getCell(6).getStringCellValue();
            String profitPrediction = lastRow.getCell(7).getStringCellValue();

            Match match = new Match(datePrediction, scorePrediction, sportPrediction, teamsPrediction);
            PredictionResult result = new PredictionResult(Integer.parseInt(profitPrediction), Outcome.fromString(outcomePrediction));

            Prediction prediction = new Prediction(match, predictionValue, result, algorithm.getId());

            file.close();
            return prediction;
        } catch (FileNotFoundException ex) {
            return new EmptyPrediction();
        } catch (Exception e) {
            e.printStackTrace();
            return new EmptyPrediction();
        } finally {
            try {
                if (workbook != null) workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private static String buildFileName(String algoId) {
        return Configs.getExcelAlgorithmsPredictionDirectory() + algoId + ".xlsx";
    }


    /**
     * This method shifts rows in a sheet from row number beginRow to row endRow (both included) by a number 'shiftValue'.
     * Shiftvalue can also be negative for shifts from bottom to top.
     *
     * @param sheet
     * @param beginRow
     * @param endRow
     * @param shiftValue
     */
    public static void shiftRows(Sheet sheet, int beginRow, int endRow, int shiftValue) {
        List<List<String>> matrix = new ArrayList<>();

        if (endRow < beginRow) return;

        //loop the rows
        for (int rowIndex = beginRow; rowIndex <= endRow; rowIndex++) {
            Row currentRow = sheet.getRow(rowIndex);
            if (currentRow == null) {
                currentRow = sheet.createRow(rowIndex);
            }

            List<String> currentRowList = new ArrayList<>();
            for (int cellIndex = 0; cellIndex < currentRow.getLastCellNum(); cellIndex++) {
                Cell cell = currentRow.getCell(cellIndex);
                String value = cell.getStringCellValue();
                currentRowList.add(value);
            }

            sheet.createRow(currentRow.getRowNum());
            matrix.add(currentRowList);
        }

        int matrixIndex = 0;
        for (int shiftedRowIndex = beginRow + shiftValue; shiftedRowIndex <= endRow + shiftValue && matrixIndex < matrix.size(); shiftedRowIndex++, matrixIndex++) {
            Row shiftedRow = sheet.getRow(shiftedRowIndex);
            if (shiftedRow == null) {
                shiftedRow = sheet.createRow(shiftedRowIndex);
            }

            List<String> rowValues = matrix.get(matrixIndex);

            for (int shiftedCellIndex = 0; shiftedCellIndex < rowValues.size(); shiftedCellIndex++) {
                String val = rowValues.get(shiftedCellIndex);
                shiftedRow.createCell(shiftedCellIndex).setCellValue(val);
            }
        }
    }

}
