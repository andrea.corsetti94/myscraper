package http;

import main.Logger;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.ConnectException;
import java.util.Random;

public class HTTPManager {

    public static Elements getHTMLElements(String url, String htmlSelector, int retry) {

        Elements select = null;
        try {

            select = Jsoup.connect(url)
                    .userAgent(randomUserAgent())
                    .referrer("http://www.google.com")
                    .get().select(htmlSelector);
        } catch (HttpStatusException | ConnectException httpException) {
            System.out.println("Connection exception for URL: \"" + url + "\"  Retrying...");
            Logger.getLogger(HTTPManager.class).error("Connection exception for URL: \"" + url + "\"  Retrying...");
            for (int i = 1; i <= retry; i++) {
                try {
                    select = Jsoup.connect(url).get().select(htmlSelector);
                    Logger.getLogger(HTTPManager.class).info("Connection executed properly after try: " + i);
                    break;
                } catch (IOException ex) {
                }
            }
        } catch (IOException e) {
            Logger.getLogger(HTTPManager.class).error("HTTP Connection still not working after " + retry + " times"
                    + " for URL: " + url);
        }
        return select;
    }

    private static String randomUserAgent() {
        String[] userAgents = new String[]{
                "Mozilla/5.0 (X11; Linux i686; rv:64.0) Gecko/20100101 Firefox/64.0",
                "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.9200",
                "Chrome (AppleWebKit/537.1; Chrome50.0; Windows NT 6.3) AppleWebKit/537.36 (KHTML like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393",
                "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:2.0) Treco/20110515 Fireweb Navigator/2.4",
                "IBM WebExplorer /v0.94",
                "Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201",
                "Opera/9.80 (Macintosh; Intel Mac OS X 10.14.1) Presto/2.12.388 Version/12.16",
                "Mozilla/5.0 (Windows NT 6.0; rv:2.0) Gecko/20100101 Firefox/4.0 Opera 12.14",
                "Opera/9.80 (Windows NT 6.0) Presto/2.12.388 Version/12.14",
                "Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A",
                "Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)",
                "Googlebot/2.1 (+http://www.googlebot.com/bot.html)",
                "Googlebot/2.1 (+http://www.google.com/bot.html)",
                "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/44.0.2403.155 Safari/537.36",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36",
                "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36",
                "Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0",
                "Mozilla/1.22 (compatible; MSIE 10.0; Windows 3.1)",
                "Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko",
                "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko"
        };

        return userAgents[new Random().nextInt(userAgents.length)];

    }
}
