package view;

class RowField {

    private RowTable rowTable;

    protected RowTable getRowTable() {
        return rowTable;
    }

    protected void setRowTable(RowTable rowTable) {
        this.rowTable = rowTable;
    }

    public RowField(RowTable rowsTable) {
        super();
        this.rowTable = rowsTable;
    }


}
