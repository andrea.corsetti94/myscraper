package view;

public class RowTable {

    private Table table;

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public RowTable(Table table) {
        super();
        this.table = table;
    }

}
