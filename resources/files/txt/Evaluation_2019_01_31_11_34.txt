Evaluation:
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38820, timestamp=2019_01_31_11_34, ROI=16.0, wins=81, losses=40, predictionSize=121, profit=1941]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38966, timestamp=2019_01_31_11_34, ROI=16.0, wins=80, losses=37, predictionSize=117, profit=1885]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38998, timestamp=2019_01_31_11_34, ROI=14.0, wins=75, losses=34, predictionSize=109, profit=1620]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39070, timestamp=2019_01_31_11_34, ROI=16.0, wins=75, losses=35, predictionSize=110, profit=1832]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39426, timestamp=2019_01_31_11_34, ROI=18.0, wins=77, losses=35, predictionSize=112, profit=2021]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39870, timestamp=2019_01_31_11_34, ROI=15.0, wins=79, losses=36, predictionSize=115, profit=1760]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40110, timestamp=2019_01_31_11_34, ROI=20.0, wins=76, losses=33, predictionSize=109, profit=2227]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40315, timestamp=2019_01_31_11_34, ROI=13.0, wins=74, losses=37, predictionSize=111, profit=1505]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40450, timestamp=2019_01_31_11_34, ROI=15.0, wins=78, losses=37, predictionSize=115, profit=1748]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40532, timestamp=2019_01_31_11_34, ROI=17.0, wins=75, losses=35, predictionSize=110, profit=1950]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-43645, timestamp=2019_01_31_11_34, ROI=15.0, wins=73, losses=35, predictionSize=108, profit=1643]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-44166, timestamp=2019_01_31_11_34, ROI=16.0, wins=78, losses=37, predictionSize=115, profit=1863]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-37831, timestamp=2019_01_31_11_34, ROI=16.0, wins=74, losses=33, predictionSize=107, profit=1788]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38072, timestamp=2019_01_31_11_34, ROI=12.0, wins=73, losses=35, predictionSize=108, profit=1346]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38095, timestamp=2019_01_31_11_34, ROI=20.0, wins=72, losses=33, predictionSize=105, profit=2109]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38107, timestamp=2019_01_31_11_34, ROI=11.0, wins=76, losses=37, predictionSize=113, profit=1338]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38456, timestamp=2019_01_31_11_34, ROI=17.0, wins=73, losses=33, predictionSize=106, profit=1838]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38934, timestamp=2019_01_31_11_34, ROI=18.0, wins=74, losses=33, predictionSize=107, profit=1944]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39069, timestamp=2019_01_31_11_34, ROI=14.0, wins=76, losses=36, predictionSize=112, profit=1679]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39315, timestamp=2019_01_31_11_34, ROI=21.0, wins=73, losses=36, predictionSize=109, profit=2374]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39566, timestamp=2019_01_31_11_34, ROI=12.0, wins=73, losses=35, predictionSize=108, profit=1350]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39683, timestamp=2019_01_31_11_34, ROI=19.0, wins=81, losses=38, predictionSize=119, profit=2376]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39764, timestamp=2019_01_31_11_34, ROI=11.0, wins=77, losses=37, predictionSize=114, profit=1343]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39898, timestamp=2019_01_31_11_34, ROI=11.0, wins=66, losses=36, predictionSize=102, profit=1200]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40465, timestamp=2019_01_31_11_34, ROI=14.0, wins=76, losses=36, predictionSize=112, profit=1593]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40494, timestamp=2019_01_31_11_34, ROI=15.0, wins=74, losses=33, predictionSize=107, profit=1637]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-41981, timestamp=2019_01_31_11_34, ROI=17.0, wins=70, losses=33, predictionSize=103, profit=1824]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-42703, timestamp=2019_01_31_11_34, ROI=14.0, wins=77, losses=38, predictionSize=115, profit=1636]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v2.0-43140, timestamp=2019_01_31_11_34, ROI=21.0, wins=75, losses=36, predictionSize=111, profit=2345]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v2.0-43169, timestamp=2019_01_31_11_34, ROI=17.0, wins=71, losses=37, predictionSize=108, profit=1911]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-44269, timestamp=2019_01_31_11_34, ROI=15.0, wins=73, losses=35, predictionSize=108, profit=1715]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-44298, timestamp=2019_01_31_11_34, ROI=17.0, wins=77, losses=37, predictionSize=114, profit=1963]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm-18033, timestamp=2019_01_31_11_34, ROI=11.0, wins=75, losses=38, predictionSize=113, profit=1250]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-37609, timestamp=2019_01_31_11_34, ROI=14.0, wins=72, losses=35, predictionSize=107, profit=1514]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-37628, timestamp=2019_01_31_11_34, ROI=10.0, wins=72, losses=39, predictionSize=111, profit=1144]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-37699, timestamp=2019_01_31_11_34, ROI=13.0, wins=75, losses=37, predictionSize=112, profit=1561]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-37826, timestamp=2019_01_31_11_34, ROI=17.0, wins=74, losses=36, predictionSize=110, profit=1871]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38124, timestamp=2019_01_31_11_34, ROI=9.0, wins=69, losses=37, predictionSize=106, profit=1037]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38729, timestamp=2019_01_31_11_34, ROI=15.0, wins=73, losses=37, predictionSize=110, profit=1713]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39018, timestamp=2019_01_31_11_34, ROI=10.0, wins=74, losses=36, predictionSize=110, profit=1204]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39204, timestamp=2019_01_31_11_34, ROI=11.0, wins=71, losses=37, predictionSize=108, profit=1209]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39269, timestamp=2019_01_31_11_34, ROI=12.0, wins=81, losses=39, predictionSize=120, profit=1546]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39285, timestamp=2019_01_31_11_34, ROI=16.0, wins=68, losses=38, predictionSize=106, profit=1710]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39430, timestamp=2019_01_31_11_34, ROI=13.0, wins=74, losses=38, predictionSize=112, profit=1476]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39798, timestamp=2019_01_31_11_34, ROI=14.0, wins=74, losses=39, predictionSize=113, profit=1631]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39972, timestamp=2019_01_31_11_34, ROI=10.0, wins=75, losses=38, predictionSize=113, profit=1218]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40263, timestamp=2019_01_31_11_34, ROI=15.0, wins=78, losses=39, predictionSize=117, profit=1815]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40580, timestamp=2019_01_31_11_34, ROI=10.0, wins=75, losses=39, predictionSize=114, profit=1218]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-41993, timestamp=2019_01_31_11_34, ROI=11.0, wins=69, losses=38, predictionSize=107, profit=1199]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-43054, timestamp=2019_01_31_11_34, ROI=11.0, wins=70, losses=36, predictionSize=106, profit=1201]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-43455, timestamp=2019_01_31_11_34, ROI=11.0, wins=71, losses=38, predictionSize=109, profit=1292]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-43909, timestamp=2019_01_31_11_34, ROI=16.0, wins=72, losses=38, predictionSize=110, profit=1854]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-44790, timestamp=2019_01_31_11_34, ROI=9.0, wins=72, losses=39, predictionSize=111, profit=1029]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-44979, timestamp=2019_01_31_11_34, ROI=15.0, wins=74, losses=34, predictionSize=108, profit=1680]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-45237, timestamp=2019_01_31_11_34, ROI=22.0, wins=74, losses=38, predictionSize=112, profit=2522]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v2-35221, timestamp=2019_01_31_11_34, ROI=15.0, wins=73, losses=39, predictionSize=112, profit=1742]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v2-35674, timestamp=2019_01_31_11_34, ROI=14.0, wins=71, losses=39, predictionSize=110, profit=1633]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v2-35985, timestamp=2019_01_31_11_34, ROI=10.0, wins=72, losses=36, predictionSize=108, profit=1151]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-37542, timestamp=2019_01_31_11_34, ROI=8.0, wins=73, losses=39, predictionSize=112, profit=975]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-37572, timestamp=2019_01_31_11_34, ROI=9.0, wins=70, losses=39, predictionSize=109, profit=996]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-37585, timestamp=2019_01_31_11_34, ROI=7.0, wins=64, losses=37, predictionSize=101, profit=736]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-37735, timestamp=2019_01_31_11_34, ROI=17.0, wins=74, losses=38, predictionSize=112, profit=1999]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-37809, timestamp=2019_01_31_11_34, ROI=15.0, wins=69, losses=36, predictionSize=105, profit=1670]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-37964, timestamp=2019_01_31_11_34, ROI=14.0, wins=72, losses=39, predictionSize=111, profit=1592]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-37987, timestamp=2019_01_31_11_34, ROI=12.0, wins=72, losses=34, predictionSize=106, profit=1293]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38022, timestamp=2019_01_31_11_34, ROI=11.0, wins=74, losses=40, predictionSize=114, profit=1278]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38047, timestamp=2019_01_31_11_34, ROI=14.0, wins=71, losses=38, predictionSize=109, profit=1580]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38085, timestamp=2019_01_31_11_34, ROI=10.0, wins=72, losses=39, predictionSize=111, profit=1197]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38202, timestamp=2019_01_31_11_34, ROI=8.0, wins=73, losses=39, predictionSize=112, profit=943]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38217, timestamp=2019_01_31_11_34, ROI=13.0, wins=74, losses=39, predictionSize=113, profit=1513]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38232, timestamp=2019_01_31_11_34, ROI=13.0, wins=73, losses=39, predictionSize=112, profit=1473]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38487, timestamp=2019_01_31_11_34, ROI=20.0, wins=72, losses=33, predictionSize=105, profit=2104]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38494, timestamp=2019_01_31_11_34, ROI=13.0, wins=73, losses=37, predictionSize=110, profit=1504]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38701, timestamp=2019_01_31_11_34, ROI=12.0, wins=73, losses=39, predictionSize=112, profit=1421]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38748, timestamp=2019_01_31_11_34, ROI=12.0, wins=72, losses=36, predictionSize=108, profit=1372]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38822, timestamp=2019_01_31_11_34, ROI=16.0, wins=67, losses=38, predictionSize=105, profit=1744]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38967, timestamp=2019_01_31_11_34, ROI=9.0, wins=71, losses=36, predictionSize=107, profit=1064]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39159, timestamp=2019_01_31_11_34, ROI=12.0, wins=75, losses=39, predictionSize=114, profit=1456]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39176, timestamp=2019_01_31_11_34, ROI=13.0, wins=74, losses=37, predictionSize=111, profit=1477]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39266, timestamp=2019_01_31_11_34, ROI=12.0, wins=74, losses=39, predictionSize=113, profit=1378]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39293, timestamp=2019_01_31_11_34, ROI=10.0, wins=74, losses=39, predictionSize=113, profit=1235]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39322, timestamp=2019_01_31_11_34, ROI=10.0, wins=73, losses=39, predictionSize=112, profit=1195]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39372, timestamp=2019_01_31_11_34, ROI=11.0, wins=74, losses=39, predictionSize=113, profit=1331]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39396, timestamp=2019_01_31_11_34, ROI=12.0, wins=72, losses=38, predictionSize=110, profit=1389]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39418, timestamp=2019_01_31_11_34, ROI=8.0, wins=71, losses=38, predictionSize=109, profit=980]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39460, timestamp=2019_01_31_11_34, ROI=11.0, wins=75, losses=40, predictionSize=115, profit=1307]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39481, timestamp=2019_01_31_11_34, ROI=13.0, wins=71, losses=37, predictionSize=108, profit=1440]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39523, timestamp=2019_01_31_11_34, ROI=11.0, wins=72, losses=36, predictionSize=108, profit=1292]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39819, timestamp=2019_01_31_11_34, ROI=10.0, wins=73, losses=36, predictionSize=109, profit=1108]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40188, timestamp=2019_01_31_11_34, ROI=11.0, wins=74, losses=38, predictionSize=112, profit=1331]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40391, timestamp=2019_01_31_11_34, ROI=14.0, wins=74, losses=34, predictionSize=108, profit=1559]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40524, timestamp=2019_01_31_11_34, ROI=11.0, wins=74, losses=39, predictionSize=113, profit=1280]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40617, timestamp=2019_01_31_11_34, ROI=12.0, wins=69, losses=36, predictionSize=105, profit=1292]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40632, timestamp=2019_01_31_11_34, ROI=12.0, wins=68, losses=36, predictionSize=104, profit=1348]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-42191, timestamp=2019_01_31_11_34, ROI=11.0, wins=73, losses=36, predictionSize=109, profit=1253]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v2.0-42237, timestamp=2019_01_31_11_34, ROI=23.0, wins=73, losses=40, predictionSize=113, profit=2687]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-42562, timestamp=2019_01_31_11_34, ROI=12.0, wins=68, losses=35, predictionSize=103, profit=1270]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-42766, timestamp=2019_01_31_11_34, ROI=21.0, wins=73, losses=38, predictionSize=111, profit=2441]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-42974, timestamp=2019_01_31_11_34, ROI=14.0, wins=67, losses=35, predictionSize=102, profit=1527]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-43568, timestamp=2019_01_31_11_34, ROI=12.0, wins=68, losses=36, predictionSize=104, profit=1304]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-43816, timestamp=2019_01_31_11_34, ROI=9.0, wins=72, losses=39, predictionSize=111, profit=1004]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-43869, timestamp=2019_01_31_11_34, ROI=13.0, wins=70, losses=38, predictionSize=108, profit=1425]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-44018, timestamp=2019_01_31_11_34, ROI=18.0, wins=70, losses=37, predictionSize=107, profit=2013]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v2.0-44041, timestamp=2019_01_31_11_34, ROI=13.0, wins=75, losses=40, predictionSize=115, profit=1517]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-44609, timestamp=2019_01_31_11_34, ROI=12.0, wins=73, losses=38, predictionSize=111, profit=1423]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-44819, timestamp=2019_01_31_11_34, ROI=12.0, wins=71, losses=37, predictionSize=108, profit=1362]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-45050, timestamp=2019_01_31_11_34, ROI=12.0, wins=71, losses=39, predictionSize=110, profit=1400]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-45269, timestamp=2019_01_31_11_34, ROI=11.0, wins=72, losses=36, predictionSize=108, profit=1196]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-45462, timestamp=2019_01_31_11_34, ROI=18.0, wins=74, losses=36, predictionSize=110, profit=2085]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v2-35126, timestamp=2019_01_31_11_34, ROI=10.0, wins=72, losses=40, predictionSize=112, profit=1130]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v2-35165, timestamp=2019_01_31_11_34, ROI=12.0, wins=72, losses=38, predictionSize=110, profit=1360]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v2-35355, timestamp=2019_01_31_11_34, ROI=10.0, wins=69, losses=38, predictionSize=107, profit=1131]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-37640, timestamp=2019_01_31_11_34, ROI=14.0, wins=69, losses=37, predictionSize=106, profit=1491]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38083, timestamp=2019_01_31_11_34, ROI=11.0, wins=70, losses=40, predictionSize=110, profit=1267]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38098, timestamp=2019_01_31_11_34, ROI=11.0, wins=75, losses=40, predictionSize=115, profit=1302]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38119, timestamp=2019_01_31_11_34, ROI=13.0, wins=70, losses=38, predictionSize=108, profit=1446]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38148, timestamp=2019_01_31_11_34, ROI=7.0, wins=71, losses=40, predictionSize=111, profit=845]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38227, timestamp=2019_01_31_11_34, ROI=11.0, wins=71, losses=35, predictionSize=106, profit=1259]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38254, timestamp=2019_01_31_11_34, ROI=9.0, wins=71, losses=39, predictionSize=110, profit=1047]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38277, timestamp=2019_01_31_11_34, ROI=13.0, wins=71, losses=35, predictionSize=106, profit=1444]
-------------------------------
