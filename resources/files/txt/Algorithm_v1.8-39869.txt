Algorithm: Algorithm_v1.8-39869
Algorithm info: Total predictions: 109
Total profit: 1917
Total wins: 77
Total lost: 32

Date: 28. Jan 19:45 - Sport: soccer - Teams: Barnet vs Brentford - Score: 3 - 3 - Prediction: Both to score - NO (1.83) - Outcome: LOST - Profit: -100
Date: 28. Jan 17:00 - Sport: soccer - Teams: Rostov vs Zenit St. Petersburg - Score: 0 - 5 - Prediction: Both to Score - YES (1.78) - Outcome: LOST - Profit: -100
Date: 28. Jan 13:00 - Sport: soccer - Teams: Irodotos vs Panachaiki - Score: 0 - 1 - Prediction: Under 2,5 goals (1.8) - Outcome: WIN - Profit: 80
Date: 27. Jan 16:00 - Sport: soccer - Teams: Crystal Palace vs Tottenham - Score: 2 - 0 - Prediction: Home or Draw (1.56) - Outcome: WIN - Profit: 56
Date: 27. Jan 15:30 - Sport: soccer - Teams: SS Monopoli vs Reggina - Score: 1 - 1 - Prediction: Draw or Away (1.5) - Outcome: WIN - Profit: 50
Date: 27. Jan 13:30 - Sport: soccer - Teams: Feyenoord vs Ajax - Score: 6 - 2 - Prediction: Over 2,5 goals (1.5) - Outcome: WIN - Profit: 50
Date: 27. Jan 13:00 - Sport: soccer - Teams: Eintracht Braunschweig vs Hansa Rostock - Score: 2 - 0 - Prediction: Under 2,5 goals (1.76) - Outcome: WIN - Profit: 76
Date: 26. Jan 18:30 - Sport: soccer - Teams: Beitar Jerusalem vs Ashdod FC - Score: 4 - 1 - Prediction: Home win (1.65) - Outcome: WIN - Profit: 64
Date: 26. Jan 17:00 - Sport: soccer - Teams: Salernitana vs Lecce - Score: 1 - 2 - Prediction: Under 2,5 goals (1.81) - Outcome: LOST - Profit: -100
Date: 26. Jan 15:00 - Sport: soccer - Teams: Exeter City vs Cambridge United - Score: 1 - 0 - Prediction: Both to Score - YES (1.86) - Outcome: LOST - Profit: -100
Date: 26. Jan 15:00 - Sport: soccer - Teams: Gainsborough Trinity vs Stafford Rangers - Score: 1 - 1 - Prediction: Both to Score - YES (1.53) - Outcome: WIN - Profit: 53
Date: 26. Jan 15:00 - Sport: soccer - Teams: UD Samano vs Textil Escudo - Score: 2 - 0 - Prediction: Under 2,5 goals (1.8) - Outcome: WIN - Profit: 80
Date: 25. Jan 19:00 - Sport: soccer - Teams: Telstar vs Sparta Rotterdam - Score: 1 - 1 - Prediction: Both to Score - YES (1.55) - Outcome: WIN - Profit: 55
Date: 23. Jan 17:30 - Sport: soccer - Teams: Olympiakos Piraeus vs Xanthi - Score: 3 - 1 - Prediction: Under 2,5 goals (1.85) - Outcome: LOST - Profit: -100
Date: 23. Jan 12:30 - Sport: soccer - Teams: Karmiotissa vs Ethnikos Achnas - Score: 2 - 1 - Prediction: Home or Draw (1.82) - Outcome: WIN - Profit: 82
Date: 22. Jan 17:30 - Sport: soccer - Teams: Villefranche vs Vendee Les Herbiers - Score: 2 - 0 - Prediction: Home win (1.81) - Outcome: WIN - Profit: 81
Date: 22. Jan 08:50 - Sport: soccer - Teams: Melbourne City vs Western Sydney Wanderers - Score: 4 - 3 - Prediction: Home win (1.81) - Outcome: WIN - Profit: 81
Date: 21. Jan 22:30 - Sport: soccer - Teams: Brazil U20 vs Venezuela U20 - Score: 2 - 1 - Prediction: Both to Score - YES (1.71) - Outcome: WIN - Profit: 71
Date: 21. Jan 20:00 - Sport: soccer - Teams: Spezia vs Venezia - Score: 1 - 1 - Prediction: Under 2,5 goals (1.63) - Outcome: WIN - Profit: 62
Date: 21. Jan 14:00 - Sport: soccer - Teams: Genoa vs AC Milan - Score: 0 - 2 - Prediction: Both to Score - YES (1.95) - Outcome: LOST - Profit: -100
Date: 21. Jan 12:00 - Sport: soccer - Teams: Genoa U19 vs Sassuolo U19 - Score: 1 - 2 - Prediction: Both to Score - YES (1.53) - Outcome: WIN - Profit: 53
Date: 20. Jan 18:00 - Sport: soccer - Teams: Bordeaux vs Dijon - Score: 1 - 0 - Prediction: Both to score - NO (1.83) - Outcome: WIN - Profit: 83
Date: 20. Jan 17:30 - Sport: soccer - Teams: Boavista vs Portimonense - Score: 0 - 2 - Prediction: Both to score - NO (1.81) - Outcome: WIN - Profit: 81
Date: 19. Jan 17:30 - Sport: soccer - Teams: AZ Alkmaar vs FC Utrecht - Score: 3 - 0 - Prediction: Over 2,5 goals (1.5) - Outcome: WIN - Profit: 50
Date: 19. Jan 15:00 - Sport: soccer - Teams: Cambridge United vs Northampton Town - Score: 3 - 2 - Prediction: Under 2,5 goals (1.87) - Outcome: LOST - Profit: -100
Date: 19. Jan 15:00 - Sport: soccer - Teams: Raith Rovers vs Dunfermline Athletic - Score: 3 - 0 - Prediction: Home or Draw (1.81) - Outcome: WIN - Profit: 81
Date: 18. Jan 20:00 - Sport: soccer - Teams: Getafe vs Deportivo Alaves - Score: 4 - 0 - Prediction: Both to score - NO (1.6) - Outcome: WIN - Profit: 60
Date: 18. Jan 19:00 - Sport: soccer - Teams: Auxerre vs Orleans - Score: 3 - 0 - Prediction: Home win (1.85) - Outcome: WIN - Profit: 85
Date: 17. Jan 13:00 - Sport: soccer - Teams: Tatran Presov vs Ban�k Ostrava - Score: 1 - 2 - Prediction: Both to Score - YES (1.85) - Outcome: WIN - Profit: 85
Date: 17. Jan 12:00 - Sport: soccer - Teams: Chemelil Sugar vs Leopards - Score: 1 - 2 - Prediction: Both to score - NO (1.67) - Outcome: LOST - Profit: -100
Date: 17. Jan 10:30 - Sport: soccer - Teams: Oxin Alborz vs Shahrdari Mahshahr - Score: 4 - 1 - Prediction: Under 2,5 goals (1.48) - Outcome: LOST - Profit: -100
Date: 16. Jan 17:30 - Sport: soccer - Teams: Cape Town City vs Free State Stars - Score: 5 - 0 - Prediction: Under 2,5 goals (1.52) - Outcome: LOST - Profit: -100
Date: 16. Jan 15:00 - Sport: soccer - Teams: ASIL vs Ermis Aradippou - Score: 0 - 4 - Prediction: Home or Draw (1.77) - Outcome: LOST - Profit: -100
Date: 15. Jan 19:45 - Sport: soccer - Teams: Blackburn Rovers vs Newcastle United - Score: 2 - 2 - Prediction: Draw or Away (1.65) - Outcome: WIN - Profit: 64
Date: 15. Jan 19:45 - Sport: soccer - Teams: Mansfield Town vs Crawley Town - Score: 1 - 0 - Prediction: Both to score - NO (1.81) - Outcome: WIN - Profit: 81
Date: 15. Jan 19:45 - Sport: soccer - Teams: Poole Town vs Swindon Supermarine - Score: 2 - 5 - Prediction: Draw or Away (1.88) - Outcome: WIN - Profit: 87
Date: 15. Jan 18:00 - Sport: soccer - Teams: Angers vs Bordeaux - Score: 1 - 2 - Prediction: Draw or Away (1.64) - Outcome: WIN - Profit: 63
Date: 14. Jan 20:15 - Sport: soccer - Teams: Famalicao vs Estoril - Score: 2 - 0 - Prediction: Under 2,5 goals (1.64) - Outcome: WIN - Profit: 63
Date: 14. Jan 19:45 - Sport: soccer - Teams: Red Star vs Lens - Score: 1 - 0 - Prediction: Both to score - NO (1.8) - Outcome: WIN - Profit: 80
Date: 14. Jan 17:00 - Sport: soccer - Teams: Alki Oroklini vs AEL Limassol - Score: 1 - 3 - Prediction: Under 2,5 goals (1.86) - Outcome: LOST - Profit: -100
Date: 13. Jan 17:00 - Sport: soccer - Teams: Lugo vs Extremadura UD - Score: 1 - 1 - Prediction: Draw or Away (1.73) - Outcome: WIN - Profit: 73
Date: 13. Jan 16:30 - Sport: soccer - Teams: Tottenham vs Manchester United - Score: 0 - 1 - Prediction: Both to Score - YES (1.55) - Outcome: LOST - Profit: -100
Date: 13. Jan 15:30 - Sport: soccer - Teams: Barcelona B vs Alcoyano - Score: 2 - 0 - Prediction: Both to score - NO (1.8) - Outcome: WIN - Profit: 80
Date: 13. Jan 14:00 - Sport: soccer - Teams: Torino vs Fiorentina - Score: 0 - 2 - Prediction: Under 2,5 goals (1.78) - Outcome: WIN - Profit: 78
Date: 13. Jan 11:15 - Sport: soccer - Teams: Benfica B vs Sporting Braga B - Score: 2 - 3 - Prediction: Home win (1.61) - Outcome: LOST - Profit: -100
Date: 13. Jan 11:00 - Sport: soccer - Teams: Sevilla C vs Algeciras CF - Score: 0 - 0 - Prediction: Under 2,5 goals (1.65) - Outcome: WIN - Profit: 64
Date: 12. Jan 23:01 - Sport: soccer - Teams: Cova da Piedade U19 vs Estoril Praia U19 - Score: 3 - 2 - Prediction: Draw or Away (1.54) - Outcome: LOST - Profit: -100
Date: 12. Jan 18:00 - Sport: soccer - Teams: Logrones UD vs Durango - Score: 1 - 0 - Prediction: Under 2,5 goals (1.65) - Outcome: WIN - Profit: 64
Date: 12. Jan 15:00 - Sport: soccer - Teams: Burgess Hill Town vs Bognor Regis Town - Score: 0 - 2 - Prediction: Both to Score - YES (1.5) - Outcome: LOST - Profit: -100
Date: 12. Jan 14:00 - Sport: soccer - Teams: Maccabi Netanya vs Ashdod FC - Score: 1 - 0 - Prediction: Under 2,5 goals (1.71) - Outcome: WIN - Profit: 71
Date: 11. Jan 21:15 - Sport: soccer - Teams: Vitoria Guimaraes vs Moreirense - Score: 1 - 0 - Prediction: Under 2,5 goals (1.83) - Outcome: WIN - Profit: 83
Date: 11. Jan 13:00 - Sport: soccer - Teams: Hapoel Katamon vs Hapoel Akko - Score: 0 - 0 - Prediction: Both to score - NO (1.62) - Outcome: WIN - Profit: 62
Date: 10. Jan 19:30 - Sport: soccer - Teams: Real Betis vs Real Sociedad - Score: 0 - 0 - Prediction: Under 2,5 goals (1.82) - Outcome: WIN - Profit: 82
Date: 10. Jan 11:30 - Sport: soccer - Teams: Churchill Brothers vs Gokulam FC - Score: 3 - 1 - Prediction: Home win (1.47) - Outcome: WIN - Profit: 47
Date: 9. Jan 18:30 - Sport: soccer - Teams: Girona vs Atletico Madrid - Score: 1 - 1 - Prediction: Under 2,5 goals (1.57) - Outcome: WIN - Profit: 57
Date: 9. Jan 17:45 - Sport: soccer - Teams: Monaco vs Rennes - Score: 1 - 1 - Prediction: Draw or Away (1.57) - Outcome: WIN - Profit: 57
Date: 9. Jan 17:30 - Sport: soccer - Teams: Kissamikos POGS vs AEK Athens - Score: 1 - 1 - Prediction: Under 2,5 goals (1.8) - Outcome: WIN - Profit: 80
Date: 9. Jan 08:50 - Sport: soccer - Teams: Adelaide United vs Melbourne Victory - Score: 2 - 0 - Prediction: Home or Draw (1.51) - Outcome: WIN - Profit: 51
Date: 8. Jan 19:45 - Sport: soccer - Teams: Northampton Town vs Bristol Rovers - Score: 1 - 2 - Prediction: Over 2,5 goals (1.82) - Outcome: WIN - Profit: 82
Date: 8. Jan 19:45 - Sport: soccer - Teams: Port Vale vs Shrewsbury Town - Score: 1 - 1 - Prediction: Both to Score - YES (1.63) - Outcome: WIN - Profit: 62
Date: 8. Jan 19:00 - Sport: soccer - Teams: Rochdale vs Man City U21 - Score: 2 - 4 - Prediction: Both to Score - YES (1.53) - Outcome: WIN - Profit: 53
Date: 8. Jan 18:00 - Sport: soccer - Teams: Amiens SC vs Angers - Score: 0 - 0 - Prediction: Home or Draw (1.45) - Outcome: WIN - Profit: 44
Date: 8. Jan 13:30 - Sport: soccer - Teams: Iraq vs Vietnam - Score: 3 - 2 - Prediction: Under 2,5 goals (1.61) - Outcome: LOST - Profit: -100
Date: 7. Jan 17:00 - Sport: soccer - Teams: Numancia vs Real Oviedo - Score: 2 - 3 - Prediction: Draw or Away (1.69) - Outcome: WIN - Profit: 69
Date: 7. Jan 11:00 - Sport: soccer - Teams: Burgos CF vs Fuenlabrada - Score: 1 - 0 - Prediction: Under 2,5 goals (1.57) - Outcome: WIN - Profit: 57
Date: 7. Jan 11:00 - Sport: soccer - Teams: Real Murcia vs Sevilla B - Score: 0 - 0 - Prediction: Home win (1.6) - Outcome: LOST - Profit: -100
Date: 6. Jan 17:00 - Sport: soccer - Teams: UD Oliveirense vs Penafiel - Score: 2 - 1 - Prediction: Home or Draw (1.55) - Outcome: WIN - Profit: 55
Date: 6. Jan 16:00 - Sport: soccer - Teams: Gimnastica Torrelavega vs CD Calahorra - Score: 1 - 1 - Prediction: Draw or Away (1.48) - Outcome: WIN - Profit: 48
Date: 6. Jan 16:00 - Sport: soccer - Teams: Talavera CF vs UCAM Murcia - Score: 0 - 0 - Prediction: Home or Draw (1.6) - Outcome: WIN - Profit: 60
Date: 5. Jan 15:00 - Sport: soccer - Teams: Boston United vs Altrincham - Score: 1 - 2 - Prediction: Both to Score - YES (1.57) - Outcome: WIN - Profit: 57
Date: 5. Jan 15:00 - Sport: soccer - Teams: Fleetwood Town vs AFC Wimbledon - Score: 2 - 3 - Prediction: Under 2,5 goals (1.79) - Outcome: LOST - Profit: -100
Date: 5. Jan 15:00 - Sport: soccer - Teams: Halifax Town vs Braintree Town - Score: 0 - 0 - Prediction: Under 2,5 goals (1.77) - Outcome: WIN - Profit: 77
Date: 5. Jan 15:00 - Sport: soccer - Teams: Wealdstone vs Dartford - Score: 1 - 1 - Prediction: Both to Score - YES (1.66) - Outcome: WIN - Profit: 65
Date: 4. Jan 20:00 - Sport: soccer - Teams: Espanyol vs Leganes - Score: 1 - 0 - Prediction: Under 2,5 goals (1.6) - Outcome: WIN - Profit: 60
Date: 4. Jan 17:00 - Sport: soccer - Teams: DRB Tadjenanet vs USM Alger - Score: 1 - 0 - Prediction: Home or Draw (1.81) - Outcome: WIN - Profit: 81
Date: 4. Jan 13:00 - Sport: soccer - Teams: Hapoel Kfar Saba vs Hapoel Bnei Lod - Score: 1 - 1 - Prediction: Home win (1.57) - Outcome: LOST - Profit: -100
Date: 3. Jan 16:30 - Sport: soccer - Teams: Al Jeel vs Al Kawkab - Score: 1 - 1 - Prediction: Draw or Away (1.67) - Outcome: WIN - Profit: 67
Date: 2. Jan 20:15 - Sport: soccer - Teams: Portimonense vs Benfica - Score: 2 - 0 - Prediction: Over 2,5 goals (1.71) - Outcome: LOST - Profit: -100
Date: 2. Jan 20:15 - Sport: soccer - Teams: Rio Ave vs Moreirense - Score: 1 - 2 - Prediction: Draw or Away (1.81) - Outcome: WIN - Profit: 81
Date: 2. Jan 17:00 - Sport: soccer - Teams: Pacos Ferreira vs Arouca - Score: 1 - 0 - Prediction: Home win (1.62) - Outcome: WIN - Profit: 62
Date: 1. Jan 15:00 - Sport: soccer - Teams: Blackpool vs Sunderland - Score: 0 - 1 - Prediction: Home or Draw (1.64) - Outcome: LOST - Profit: -100
Date: 1. Jan 15:00 - Sport: soccer - Teams: Dartford vs Welling United - Score: 1 - 0 - Prediction: Draw or Away (1.46) - Outcome: LOST - Profit: -100
Date: 1. Jan 15:00 - Sport: soccer - Teams: Hyde United FC vs Buxton - Score: 1 - 1 - Prediction: Draw or Away (1.54) - Outcome: WIN - Profit: 54
Date: 1. Jan 15:00 - Sport: soccer - Teams: Ipswich Town vs Millwall - Score: 2 - 3 - Prediction: Home or Draw (1.66) - Outcome: LOST - Profit: -100
Date: 31. Dec 11:00 - Sport: soccer - Teams: Philippines vs Vietnam - Score: 0 - 0 - Prediction: Under 2,5 goals (1.77) - Outcome: WIN - Profit: 77
Date: 31. Dec 08:00 - Sport: soccer - Teams: Central Coast Mariners vs Perth Glory - Score: 1 - 4 - Prediction: Away win (1.8) - Outcome: WIN - Profit: 80
Date: 30. Dec 17:00 - Sport: soccer - Teams: Hapoel Eran Hadera vs Maccabi Petah Tikva - Score: 3 - 0 - Prediction: Home or Draw (1.65) - Outcome: WIN - Profit: 64
Date: 30. Dec 14:00 - Sport: soccer - Teams: Venezia vs Carpi - Score: 1 - 1 - Prediction: Both to score - NO (1.65) - Outcome: LOST - Profit: -100
Date: 30. Dec 12:00 - Sport: soccer - Teams: Oman vs Australia - Score: 0 - 5 - Prediction: Both to score - NO (1.53) - Outcome: WIN - Profit: 53
Date: 29. Dec 15:00 - Sport: soccer - Teams: Montrose vs Arbroath - Score: 1 - 1 - Prediction: Both to Score - YES (1.57) - Outcome: WIN - Profit: 57
Date: 29. Dec 15:00 - Sport: soccer - Teams: Southport vs Alfreton Town - Score: 2 - 1 - Prediction: Draw or Away (1.94) - Outcome: LOST - Profit: -100
Date: 29. Dec 15:00 - Sport: soccer - Teams: West Bromwich Albion vs Sheffield Wednesday - Score: 1 - 1 - Prediction: Home win (1.53) - Outcome: LOST - Profit: -100
Date: 28. Dec 10:45 - Sport: soccer - Teams: Nordia Jerusalem vs Beitar Kfar Saba Shlomi - Score: 2 - 1 - Prediction: Home win (1.78) - Outcome: WIN - Profit: 78
Date: 28. Dec 08:50 - Sport: soccer - Teams: Melbourne Victory vs Wellington Phoenix - Score: 1 - 1 - Prediction: Over 2,5 goals (1.58) - Outcome: LOST - Profit: -100
Date: 28. Dec 08:30 - Sport: soccer - Teams: NEROCA vs Mohun Bagan - Score: 2 - 1 - Prediction: Under 2,5 goals (1.61) - Outcome: LOST - Profit: -100
Date: 27. Dec 19:45 - Sport: soccer - Teams: Southampton vs West Ham United - Score: 1 - 2 - Prediction: Draw or Away (1.76) - Outcome: WIN - Profit: 76
Date: 27. Dec 17:00 - Sport: soccer - Teams: Beitar Tel Aviv vs Hapoel Katamon - Score: 2 - 0 - Prediction: Home or Away (1.44) - Outcome: WIN - Profit: 43
Date: 27. Dec 13:00 - Sport: soccer - Teams: US Tataouine vs Club Africain - Score: 0 - 2 - Prediction: Under 2,5 goals (1.52) - Outcome: WIN - Profit: 52
Date: 26. Dec 16:30 - Sport: soccer - Teams: ASD Albissola 2010 vs Alessandria - Score: 1 - 2 - Prediction: Draw or Away (1.44) - Outcome: WIN - Profit: 43
Date: 26. Dec 15:00 - Sport: soccer - Teams: Carlisle United vs Oldham Athletic - Score: 6 - 0 - Prediction: Both to Score - YES (1.8) - Outcome: LOST - Profit: -100
Date: 26. Dec 15:00 - Sport: soccer - Teams: Millwall vs Reading - Score: 1 - 0 - Prediction: Home win (1.75) - Outcome: WIN - Profit: 75
Date: 26. Dec 13:00 - Sport: soccer - Teams: Bishops Stortford vs Harlow Town - Score: 1 - 0 - Prediction: Home win (1.7) - Outcome: WIN - Profit: 70
Date: 25. Dec 17:00 - Sport: soccer - Teams: Hapoel Iksal Imad vs Hapoel Ashkelon - Score: 2 - 0 - Prediction: Home or Draw (1.59) - Outcome: WIN - Profit: 59
Date: 25. Dec 17:00 - Sport: soccer - Teams: Qatar vs Kyrgyzstan - Score: 1 - 0 - Prediction: Under 2,5 goals (1.73) - Outcome: WIN - Profit: 73
Date: 24. Dec 17:00 - Sport: soccer - Teams: Akhisar Belediyespor vs Konyaspor - Score: 0 - 0 - Prediction: Home or Draw (1.46) - Outcome: WIN - Profit: 46
Date: 23. Dec 17:30 - Sport: soccer - Teams: El Geish vs Nogoom El Mostakbal - Score: 2 - 1 - Prediction: Under 2,5 goals (1.75) - Outcome: LOST - Profit: -100
Date: 23. Dec 16:30 - Sport: soccer - Teams: Spezia vs Palermo - Score: 1 - 1 - Prediction: Under 2,5 goals (1.58) - Outcome: WIN - Profit: 58
Date: 23. Dec 14:00 - Sport: soccer - Teams: Venezia vs Cosenza Calcio - Score: 0 - 1 - Prediction: Both to score - NO (1.73) - Outcome: WIN - Profit: 73
Date: 23. Dec 13:30 - Sport: soccer - Teams: Juve Stabia vs Matera - Score: 4 - 0 - Prediction: Under 2,5 goals (1.81) - Outcome: LOST - Profit: -100
