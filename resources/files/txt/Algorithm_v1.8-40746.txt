Algorithm: Algorithm_v1.8-40746
Algorithm info: Total predictions: 109
Total profit: 2601
Total wins: 81
Total lost: 28

Date: 28. Jan 19:00 - Sport: soccer - Teams: Jong Ajax vs FC Volendam - Score: 1 - 3 - Prediction: Home win (1.49) - Outcome: LOST - Profit: -100
Date: 28. Jan 18:00 - Sport: soccer - Teams: Clermont Foot vs Le Havre - Score: 0 - 0 - Prediction: Under 2,5 goals (1.6) - Outcome: WIN - Profit: 60
Date: 28. Jan 14:00 - Sport: soccer - Teams: Iran vs Japan - Score: 0 - 3 - Prediction: Draw or Away (1.67) - Outcome: WIN - Profit: 67
Date: 27. Jan 13:30 - Sport: soccer - Teams: Arzachena vs Arezzo - Score: 0 - 1 - Prediction: Home or Draw (1.87) - Outcome: LOST - Profit: -100
Date: 27. Jan 13:30 - Sport: soccer - Teams: Catanzaro vs Casertana - Score: 3 - 2 - Prediction: Home win (1.95) - Outcome: WIN - Profit: 95
Date: 27. Jan 13:30 - Sport: soccer - Teams: Lucchese vs Alessandria - Score: 2 - 2 - Prediction: Draw or Away (1.49) - Outcome: WIN - Profit: 49
Date: 26. Jan 15:00 - Sport: soccer - Teams: Greenock Morton vs Dunfermline Athletic - Score: 0 - 0 - Prediction: Under 2,5 goals (1.83) - Outcome: WIN - Profit: 83
Date: 26. Jan 15:00 - Sport: soccer - Teams: Manchester City vs Burnley - Score: 5 - 0 - Prediction: Both to score - NO (1.73) - Outcome: WIN - Profit: 73
Date: 26. Jan 15:00 - Sport: soccer - Teams: Queens Park vs Berwick Rangers - Score: 7 - 1 - Prediction: Both to Score - YES (1.85) - Outcome: WIN - Profit: 85
Date: 26. Jan 15:00 - Sport: soccer - Teams: Scarborough Athletic vs Farsley Celtic - Score: 1 - 3 - Prediction: Both to Score - YES (1.55) - Outcome: WIN - Profit: 55
Date: 25. Jan 19:00 - Sport: soccer - Teams: NAC Breda vs ADO Den Haag - Score: 1 - 1 - Prediction: Over 2,5 goals (1.81) - Outcome: LOST - Profit: -100
Date: 25. Jan 13:00 - Sport: soccer - Teams: Beitar Tel Aviv vs Maccabi Ahi Nazareth - Score: 3 - 0 - Prediction: Both to score - NO (1.75) - Outcome: WIN - Profit: 75
Date: 25. Jan 13:00 - Sport: soccer - Teams: Hapoel Iksal Imad vs Hapoel Petah Tikva - Score: 0 - 0 - Prediction: Under 2,5 goals (1.62) - Outcome: WIN - Profit: 62
Date: 23. Jan 19:45 - Sport: soccer - Teams: Motherwell vs Hibernian - Score: 1 - 0 - Prediction: Home or Draw (1.72) - Outcome: WIN - Profit: 72
Date: 23. Jan 19:45 - Sport: soccer - Teams: Sporting Braga vs Sporting CP - Score: 1 - 1 - Prediction: Home or Draw (1.47) - Outcome: WIN - Profit: 47
Date: 23. Jan 19:30 - Sport: soccer - Teams: Sicula Leonzio vs Trapani - Score: 0 - 0 - Prediction: Home or Draw (1.67) - Outcome: WIN - Profit: 67
Date: 22. Jan 19:45 - Sport: soccer - Teams: Alvechurch vs Stratford Town - Score: 2 - 2 - Prediction: Both to Score - YES (1.54) - Outcome: WIN - Profit: 54
Date: 22. Jan 19:45 - Sport: soccer - Teams: Guiseley AFC vs Hereford FC - Score: 1 - 1 - Prediction: Over 2,5 goals (1.8) - Outcome: LOST - Profit: -100
Date: 22. Jan 17:30 - Sport: soccer - Teams: Orlando Pirates vs Baroka FC - Score: 1 - 1 - Prediction: Under 2,5 goals (1.66) - Outcome: WIN - Profit: 65
Date: 22. Jan 16:45 - Sport: soccer - Teams: MC Oran vs JS Saoura - Score: 1 - 1 - Prediction: Under 2,5 goals (1.57) - Outcome: WIN - Profit: 57
Date: 22. Jan 13:00 - Sport: soccer - Teams: Millwall U23 vs Crystal Palace U23 - Score: 1 - 2 - Prediction: Draw or Away (1.66) - Outcome: WIN - Profit: 65
Date: 22. Jan 13:00 - Sport: soccer - Teams: Velez Mostar vs Mladost Doboj Kakanj - Score: 3 - 4 - Prediction: Over 2,5 goals (1.85) - Outcome: WIN - Profit: 85
Date: 21. Jan 17:00 - Sport: soccer - Teams: Lamia vs Panathinaikos - Score: 1 - 0 - Prediction: Home or Draw (1.52) - Outcome: WIN - Profit: 52
Date: 21. Jan 14:00 - Sport: soccer - Teams: Australia vs Uzbekistan - Score: 0 - 0 - Prediction: Both to score - NO (1.79) - Outcome: WIN - Profit: 79
Date: 20. Jan 17:30 - Sport: soccer - Teams: Casertana vs FC Rieti - Score: 2 - 1 - Prediction: Under 2,5 goals (1.65) - Outcome: LOST - Profit: -100
Date: 20. Jan 15:00 - Sport: soccer - Teams: Sporting Gijon vs Alcorcon - Score: 2 - 0 - Prediction: Both to score - NO (1.61) - Outcome: WIN - Profit: 61
Date: 20. Jan 13:30 - Sport: soccer - Teams: Polokwane City vs Bidvest Wits - Score: 3 - 3 - Prediction: Draw or Away (1.52) - Outcome: WIN - Profit: 52
Date: 19. Jan 19:00 - Sport: soccer - Teams: Constantine vs TP Mazembe Englebert - Score: 3 - 0 - Prediction: Both to score - NO (1.6) - Outcome: WIN - Profit: 60
Date: 19. Jan 15:00 - Sport: soccer - Teams: Bristol Rovers vs Wycombe Wanderers - Score: 0 - 1 - Prediction: Draw or Away (1.87) - Outcome: WIN - Profit: 87
Date: 19. Jan 15:00 - Sport: soccer - Teams: Watford vs Burnley - Score: 0 - 0 - Prediction: Home win (1.7) - Outcome: LOST - Profit: -100
Date: 19. Jan 10:30 - Sport: soccer - Teams: Yeni Malatyaspor vs Goztepe - Score: 3 - 2 - Prediction: Under 2,5 goals (1.76) - Outcome: LOST - Profit: -100
Date: 18. Jan 20:00 - Sport: soccer - Teams: Palermo vs Salernitana - Score: 1 - 2 - Prediction: Home win (1.7) - Outcome: LOST - Profit: -100
Date: 18. Jan 08:50 - Sport: soccer - Teams: Western Sydney Wanderers vs Adelaide United - Score: 1 - 2 - Prediction: Both to Score - YES (1.58) - Outcome: WIN - Profit: 58
Date: 17. Jan 19:30 - Sport: soccer - Teams: Bourg-Bresse vs Laval - Score: 2 - 1 - Prediction: Both to score - NO (1.75) - Outcome: LOST - Profit: -100
Date: 17. Jan 18:00 - Sport: soccer - Teams: Portugal (w) vs Ukraine (w) - Score: 1 - 1 - Prediction: Both to Score - YES (1.92) - Outcome: WIN - Profit: 92
Date: 17. Jan 15:00 - Sport: soccer - Teams: Luzern vs Steaua Bucuresti - Score: 0 - 0 - Prediction: Home or Draw (1.57) - Outcome: WIN - Profit: 57
Date: 17. Jan 12:00 - Sport: soccer - Teams: Zoo Kericho vs Kariobangi Sharks - Score: 1 - 1 - Prediction: Home or Draw (1.51) - Outcome: WIN - Profit: 51
Date: 16. Jan 17:30 - Sport: soccer - Teams: Mamelodi Sundowns vs Maritzburg United - Score: 1 - 0 - Prediction: Under 2,5 goals (1.64) - Outcome: WIN - Profit: 63
Date: 15. Jan 20:30 - Sport: soccer - Teams: Valencia vs Sporting Gijon - Score: 3 - 0 - Prediction: Over 2,5 goals (1.7) - Outcome: WIN - Profit: 70
Date: 15. Jan 19:45 - Sport: soccer - Teams: Luton Town vs Sheffield Wednesday - Score: 0 - 1 - Prediction: Draw or Away (1.66) - Outcome: WIN - Profit: 65
Date: 15. Jan 18:00 - Sport: soccer - Teams: Desportivo Aves vs Sporting Braga - Score: 1 - 2 - Prediction: Away win (1.79) - Outcome: WIN - Profit: 79
Date: 15. Jan 08:50 - Sport: soccer - Teams: Brisbane Roar vs Melbourne Victory - Score: 0 - 5 - Prediction: Over 2,5 goals (1.6) - Outcome: WIN - Profit: 60
Date: 14. Jan 19:45 - Sport: soccer - Teams: Red Star vs Lens - Score: 1 - 0 - Prediction: Both to score - NO (1.8) - Outcome: WIN - Profit: 80
Date: 14. Jan 17:00 - Sport: soccer - Teams: Alki Oroklini vs AEL Limassol - Score: 1 - 3 - Prediction: Under 2,5 goals (1.86) - Outcome: LOST - Profit: -100
Date: 14. Jan 17:00 - Sport: soccer - Teams: Aris Thessaloniki vs Lamia - Score: 1 - 0 - Prediction: Home win (1.6) - Outcome: WIN - Profit: 60
Date: 13. Jan 17:00 - Sport: soccer - Teams: Real Zaragoza vs Malaga - Score: 0 - 2 - Prediction: Draw or Away (1.48) - Outcome: WIN - Profit: 48
Date: 13. Jan 16:00 - Sport: soccer - Teams: Pontevedra vs Unionistas de Salamanca - Score: 0 - 0 - Prediction: Under 2,5 goals (1.72) - Outcome: WIN - Profit: 72
Date: 13. Jan 13:30 - Sport: soccer - Teams: Roda JC vs Go Ahead Eagles - Score: 3 - 1 - Prediction: Home or Draw (1.58) - Outcome: WIN - Profit: 58
Date: 13. Jan 13:00 - Sport: soccer - Teams: St. Andrews FC vs Mosta - Score: 1 - 1 - Prediction: Draw or Away (1.54) - Outcome: WIN - Profit: 54
Date: 13. Jan 11:00 - Sport: soccer - Teams: Marbella vs Almeria B - Score: 2 - 0 - Prediction: Under 2,5 goals (1.57) - Outcome: WIN - Profit: 57
Date: 12. Jan 17:30 - Sport: soccer - Teams: OFI Crete vs Panionios - Score: 1 - 1 - Prediction: Under 2,5 goals (1.53) - Outcome: WIN - Profit: 53
Date: 12. Jan 15:00 - Sport: soccer - Teams: Birmingham City vs Middlesbrough - Score: 1 - 2 - Prediction: Draw or Away (1.52) - Outcome: WIN - Profit: 52
Date: 12. Jan 15:00 - Sport: soccer - Teams: Kidderminster Harriers vs Boston United - Score: 1 - 2 - Prediction: Over 2,5 goals (1.73) - Outcome: WIN - Profit: 73
Date: 12. Jan 15:00 - Sport: soccer - Teams: Montrose vs East Fife - Score: 0 - 2 - Prediction: Draw or Away (1.6) - Outcome: WIN - Profit: 60
Date: 12. Jan 15:00 - Sport: soccer - Teams: Wrexham vs Leyton Orient - Score: 0 - 1 - Prediction: Draw or Away (1.52) - Outcome: WIN - Profit: 52
Date: 11. Jan 19:00 - Sport: soccer - Teams: Clermont Foot vs Niort - Score: 3 - 2 - Prediction: Under 2,5 goals (1.72) - Outcome: LOST - Profit: -100
Date: 11. Jan 19:00 - Sport: soccer - Teams: Lorient vs Chateauroux - Score: 2 - 1 - Prediction: Home win (1.67) - Outcome: WIN - Profit: 67
Date: 11. Jan 13:00 - Sport: soccer - Teams: Hapoel Nazareth Illit vs Maccabi Ahi Nazareth - Score: 0 - 0 - Prediction: Home win (1.62) - Outcome: LOST - Profit: -100
Date: 11. Jan 08:50 - Sport: soccer - Teams: Melbourne City vs Brisbane Roar - Score: 1 - 0 - Prediction: Both to Score - YES (1.74) - Outcome: LOST - Profit: -100
Date: 10. Jan 18:30 - Sport: soccer - Teams: Athletic Bilbao vs Sevilla - Score: 1 - 3 - Prediction: Both to Score - YES (1.8) - Outcome: WIN - Profit: 80
Date: 10. Jan 13:30 - Sport: soccer - Teams: Express vs Ndejje University FC - Score: 0 - 0 - Prediction: Both to score - NO (1.55) - Outcome: WIN - Profit: 55
Date: 10. Jan 11:00 - Sport: soccer - Teams: Bahrain vs Thailand - Score: 0 - 1 - Prediction: Draw or Away (1.73) - Outcome: WIN - Profit: 73
Date: 9. Jan 19:00 - Sport: soccer - Teams: Chelsea U21 vs Peterborough United - Score: 1 - 3 - Prediction: Draw or Away (1.61) - Outcome: WIN - Profit: 61
Date: 9. Jan 17:30 - Sport: soccer - Teams: Maritzburg United vs Free State Stars - Score: 1 - 2 - Prediction: Under 2,5 goals (1.5) - Outcome: LOST - Profit: -100
Date: 8. Jan 19:45 - Sport: soccer - Teams: Brightlingsea Regent vs Bognor Regis Town - Score: 3 - 0 - Prediction: Home or Draw (1.76) - Outcome: WIN - Profit: 76
Date: 8. Jan 16:00 - Sport: soccer - Teams: Saudi Arabia vs Korea DPR - Score: 4 - 0 - Prediction: Home win (1.67) - Outcome: WIN - Profit: 67
Date: 8. Jan 12:00 - Sport: soccer - Teams: Tatran Presov vs Poprad FK - Score: 3 - 0 - Prediction: Home or Draw (2.83) - Outcome: WIN - Profit: 183
Date: 7. Jan 20:00 - Sport: soccer - Teams: Celta de Vigo vs Athletic Bilbao - Score: 1 - 2 - Prediction: Both to Score - YES (1.77) - Outcome: WIN - Profit: 77
Date: 7. Jan 16:00 - Sport: soccer - Teams: Iran vs Yemen - Score: 5 - 0 - Prediction: Over 2,5 goals (1.54) - Outcome: WIN - Profit: 54
Date: 7. Jan 11:00 - Sport: soccer - Teams: Linense vs FC Cartagena - Score: 0 - 1 - Prediction: Both to score - NO (1.83) - Outcome: WIN - Profit: 83
Date: 6. Jan 17:30 - Sport: soccer - Teams: Gernika Club vs Barakaldo - Score: 0 - 1 - Prediction: Home or Draw (1.49) - Outcome: LOST - Profit: -100
Date: 6. Jan 16:00 - Sport: soccer - Teams: El Ejido vs Malaga II - Score: 0 - 0 - Prediction: Home win (1.65) - Outcome: LOST - Profit: -100
Date: 6. Jan 16:00 - Sport: soccer - Teams: Talavera CF vs UCAM Murcia - Score: 0 - 0 - Prediction: Under 2,5 goals (1.61) - Outcome: WIN - Profit: 61
Date: 5. Jan 17:30 - Sport: soccer - Teams: Crystal Palace vs Grimsby Town - Score: 1 - 0 - Prediction: Over 2,5 goals (1.5) - Outcome: LOST - Profit: -100
Date: 5. Jan 15:00 - Sport: soccer - Teams: Boston United vs Altrincham - Score: 1 - 2 - Prediction: Over 2,5 goals (1.68) - Outcome: WIN - Profit: 68
Date: 5. Jan 15:00 - Sport: soccer - Teams: Gateshead vs Maidenhead United - Score: 0 - 1 - Prediction: Home win (1.7) - Outcome: LOST - Profit: -100
Date: 5. Jan 15:00 - Sport: soccer - Teams: Havant and Waterlooville vs Barrow AFC - Score: 2 - 0 - Prediction: Both to Score - YES (1.75) - Outcome: LOST - Profit: -100
Date: 4. Jan 19:55 - Sport: soccer - Teams: Nantes vs Chateauroux - Score: 4 - 1 - Prediction: Home win (1.51) - Outcome: WIN - Profit: 51
Date: 4. Jan 18:00 - Sport: soccer - Teams: Chippa United vs Bloemfontein Celtic - Score: 0 - 1 - Prediction: Both to score - NO (1.75) - Outcome: WIN - Profit: 75
Date: 4. Jan 14:00 - Sport: soccer - Teams: Bordj Bou Arreridj vs MC Oran - Score: 3 - 1 - Prediction: Home or Draw (1.54) - Outcome: WIN - Profit: 54
Date: 4. Jan 13:00 - Sport: soccer - Teams: Beitar Tel Aviv vs Hapoel Ashkelon - Score: 2 - 0 - Prediction: Draw or Away (1.5) - Outcome: LOST - Profit: -100
Date: 3. Jan 18:00 - Sport: soccer - Teams: Sporting CP vs Belenenses - Score: 2 - 1 - Prediction: Both to score - NO (1.87) - Outcome: LOST - Profit: -100
Date: 2. Jan 16:00 - Sport: soccer - Teams: Apollon Limassol vs Omonia Nicosia - Score: 1 - 0 - Prediction: Both to score - NO (1.73) - Outcome: WIN - Profit: 73
Date: 2. Jan 12:00 - Sport: soccer - Teams: Posta Rangers vs Leopards - Score: 0 - 2 - Prediction: Draw or Away (1.45) - Outcome: WIN - Profit: 44
Date: 2. Jan 08:50 - Sport: soccer - Teams: Newcastle Jets vs Brisbane Roar - Score: 2 - 2 - Prediction: Over 2,5 goals (1.85) - Outcome: WIN - Profit: 85
Date: 1. Jan 15:00 - Sport: soccer - Teams: Hull City vs Bolton Wanderers - Score: 6 - 0 - Prediction: Home win (1.7) - Outcome: WIN - Profit: 70
Date: 1. Jan 15:00 - Sport: soccer - Teams: Sheffield Wednesday vs Birmingham City - Score: 1 - 1 - Prediction: Draw or Away (1.48) - Outcome: WIN - Profit: 48
Date: 1. Jan 15:00 - Sport: soccer - Teams: Weymouth vs Poole Town - Score: 2 - 2 - Prediction: Both to Score - YES (1.5) - Outcome: WIN - Profit: 50
Date: 31. Dec 08:00 - Sport: soccer - Teams: Central Coast Mariners vs Perth Glory - Score: 1 - 4 - Prediction: Away win (1.8) - Outcome: WIN - Profit: 80
Date: 30. Dec 17:00 - Sport: soccer - Teams: Livorno vs Calcio Padova - Score: 1 - 1 - Prediction: Under 2,5 goals (1.67) - Outcome: WIN - Profit: 67
Date: 30. Dec 14:00 - Sport: soccer - Teams: Raja Casablanca Atlhletic vs RSB Berkane - Score: 1 - 1 - Prediction: Under 2,5 goals (1.63) - Outcome: WIN - Profit: 62
Date: 29. Dec 15:00 - Sport: soccer - Teams: Chippenham Town vs Hampton and Richmond - Score: 3 - 2 - Prediction: Over 2,5 goals (1.8) - Outcome: WIN - Profit: 80
Date: 29. Dec 15:00 - Sport: soccer - Teams: Ebbsfleet United vs Eastleigh - Score: 3 - 0 - Prediction: Draw or Away (1.75) - Outcome: LOST - Profit: -100
Date: 29. Dec 15:00 - Sport: soccer - Teams: Mickleover Sports vs Marine - Score: 1 - 0 - Prediction: Draw or Away (1.55) - Outcome: LOST - Profit: -100
Date: 29. Dec 08:50 - Sport: soccer - Teams: Sydney FC vs Brisbane Roar - Score: 2 - 1 - Prediction: Home win (1.53) - Outcome: WIN - Profit: 53
Date: 28. Dec 19:00 - Sport: soccer - Teams: Vitoria Setubal vs Sporting Braga - Score: 0 - 4 - Prediction: Away win (1.77) - Outcome: WIN - Profit: 77
Date: 28. Dec 11:30 - Sport: soccer - Teams: East Bengal vs Real Kashmir - Score: 1 - 1 - Prediction: Under 2,5 goals (1.68) - Outcome: WIN - Profit: 68
Date: 28. Dec 11:30 - Sport: soccer - Teams: Maccabi Herzliya vs Maccabi Yavne - Score: 0 - 0 - Prediction: Draw or Away (1.5) - Outcome: WIN - Profit: 50
Date: 27. Dec 19:45 - Sport: soccer - Teams: Southampton vs West Ham United - Score: 1 - 2 - Prediction: Both to Score - YES (1.65) - Outcome: WIN - Profit: 64
Date: 27. Dec 17:00 - Sport: soccer - Teams: Beitar Tel Aviv vs Hapoel Katamon - Score: 2 - 0 - Prediction: Draw or Away (1.59) - Outcome: LOST - Profit: -100
Date: 27. Dec 13:30 - Sport: soccer - Teams: Ternana vs Teramo - Score: 2 - 1 - Prediction: Both to score - NO (1.67) - Outcome: LOST - Profit: -100
Date: 26. Dec 15:00 - Sport: soccer - Teams: Chelmsford City vs Billericay Town - Score: 5 - 1 - Prediction: Home or Draw (1.45) - Outcome: WIN - Profit: 44
Date: 26. Dec 14:00 - Sport: soccer - Teams: Cagliari vs Genoa - Score: 1 - 0 - Prediction: Draw or Away (1.69) - Outcome: LOST - Profit: -100
Date: 25. Dec 17:00 - Sport: soccer - Teams: Hapoel Kfar Saba vs Hapoel Afula - Score: 1 - 1 - Prediction: Under 2,5 goals (1.67) - Outcome: WIN - Profit: 67
Date: 25. Dec 17:00 - Sport: soccer - Teams: Hapoel Nazareth Illit vs Hapoel Nir Ramat HaSharon - Score: 0 - 0 - Prediction: Both to score - NO (1.83) - Outcome: WIN - Profit: 83
Date: 24. Dec 17:00 - Sport: soccer - Teams: Akhisar Belediyespor vs Konyaspor - Score: 0 - 0 - Prediction: Under 2,5 goals (1.8) - Outcome: WIN - Profit: 80
Date: 23. Dec 20:00 - Sport: soccer - Teams: Benevento vs Crotone - Score: 3 - 0 - Prediction: Under 2,5 goals (1.79) - Outcome: LOST - Profit: -100
Date: 23. Dec 12:00 - Sport: soccer - Teams: CD Buzanada vs Union Sur Yaiza - Score: 4 - 1 - Prediction: Draw or Away (1.56) - Outcome: LOST - Profit: -100
Date: 22. Dec 15:00 - Sport: soccer - Teams: Real Oviedo vs Malaga - Score: 0 - 0 - Prediction: Draw or Away (1.5) - Outcome: WIN - Profit: 50
