Evaluation:
AlgorithmEvaluation [algorithm=Algorithm_v1.8-41861, timestamp=2019_01_31_09_48, ROI=29.0, wins=86, losses=30, predictionSize=116, profit=3387]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39939, timestamp=2019_01_31_09_48, ROI=20.0, wins=86, losses=32, predictionSize=118, profit=2444]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40746, timestamp=2019_01_31_09_48, ROI=21.0, wins=85, losses=32, predictionSize=117, profit=2475]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v2-35181, timestamp=2019_01_31_09_48, ROI=25.0, wins=82, losses=32, predictionSize=114, profit=2909]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39706, timestamp=2019_01_31_09_48, ROI=21.0, wins=83, losses=34, predictionSize=117, profit=2467]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39809, timestamp=2019_01_31_09_48, ROI=23.0, wins=84, losses=33, predictionSize=117, profit=2745]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38420, timestamp=2019_01_31_09_48, ROI=22.0, wins=86, losses=35, predictionSize=121, profit=2664]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38755, timestamp=2019_01_31_09_48, ROI=23.0, wins=80, losses=34, predictionSize=114, profit=2715]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39653, timestamp=2019_01_31_09_48, ROI=17.0, wins=83, losses=36, predictionSize=119, profit=2050]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39869, timestamp=2019_01_31_09_48, ROI=17.0, wins=82, losses=35, predictionSize=117, profit=1998]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-42101, timestamp=2019_01_31_09_48, ROI=19.0, wins=81, losses=34, predictionSize=115, profit=2224]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-42984, timestamp=2019_01_31_09_48, ROI=20.0, wins=80, losses=34, predictionSize=114, profit=2331]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v2-35705, timestamp=2019_01_31_09_48, ROI=21.0, wins=72, losses=31, predictionSize=103, profit=2252]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38356, timestamp=2019_01_31_09_48, ROI=19.0, wins=77, losses=32, predictionSize=109, profit=2144]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40086, timestamp=2019_01_31_09_48, ROI=19.0, wins=80, losses=32, predictionSize=112, profit=2172]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38643, timestamp=2019_01_31_09_48, ROI=22.0, wins=76, losses=31, predictionSize=107, profit=2395]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38685, timestamp=2019_01_31_09_48, ROI=14.0, wins=75, losses=36, predictionSize=111, profit=1560]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-39680, timestamp=2019_01_31_09_48, ROI=16.0, wins=76, losses=36, predictionSize=112, profit=1832]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-40240, timestamp=2019_01_31_09_48, ROI=19.0, wins=73, losses=32, predictionSize=105, profit=2039]
-------------------------------
AlgorithmEvaluation [algorithm=Algorithm_v1.8-38118, timestamp=2019_01_31_09_48, ROI=19.0, wins=78, losses=34, predictionSize=112, profit=2215]
-------------------------------
