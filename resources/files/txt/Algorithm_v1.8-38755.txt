Algorithm: Algorithm_v1.8-38755
Algorithm info: Total predictions: 105
Total profit: 2577
Total wins: 74
Total lost: 31

Date: 28. Jan 19:00 - Sport: soccer - Teams: Hapoel Eran Hadera vs Maccabi Tel Aviv - Score: 0 - 2 - Prediction: Both to score - NO (1.7) - Outcome: WIN - Profit: 70
Date: 28. Jan 19:00 - Sport: soccer - Teams: Tondela vs Desportivo Aves - Score: 0 - 2 - Prediction: Draw or Away (1.85) - Outcome: WIN - Profit: 85
Date: 27. Jan 19:30 - Sport: soccer - Teams: Catania vs SS Rende - Score: 1 - 0 - Prediction: Home win (1.56) - Outcome: WIN - Profit: 56
Date: 27. Jan 13:30 - Sport: soccer - Teams: St. Mirren vs Hibernian - Score: 1 - 3 - Prediction: Away win (1.8) - Outcome: WIN - Profit: 80
Date: 27. Jan 11:15 - Sport: soccer - Teams: Fortuna Sittard vs Vitesse - Score: 2 - 1 - Prediction: Over 2,5 goals (1.7) - Outcome: WIN - Profit: 70
Date: 26. Jan 15:00 - Sport: soccer - Teams: Dartford vs Concord Rangers - Score: 1 - 1 - Prediction: Under 2,5 goals (1.97) - Outcome: WIN - Profit: 97
Date: 26. Jan 15:00 - Sport: soccer - Teams: Doncaster Rovers vs Oldham Athletic - Score: 2 - 1 - Prediction: Home win (1.83) - Outcome: WIN - Profit: 83
Date: 26. Jan 15:00 - Sport: soccer - Teams: Grimsby Town vs Milton Keynes Dons - Score: 1 - 0 - Prediction: Away win (1.95) - Outcome: LOST - Profit: -100
Date: 26. Jan 15:00 - Sport: soccer - Teams: Leamington vs Altrincham - Score: 3 - 0 - Prediction: Home or Draw (1.7) - Outcome: WIN - Profit: 70
Date: 26. Jan 15:00 - Sport: soccer - Teams: Nottingham Forest vs Wigan Athletic - Score: 3 - 1 - Prediction: Both to score - NO (1.91) - Outcome: LOST - Profit: -100
Date: 26. Jan 15:00 - Sport: soccer - Teams: Portsmouth vs Queens Park Rangers - Score: 1 - 1 - Prediction: Draw or Away (1.64) - Outcome: WIN - Profit: 63
Date: 25. Jan 19:30 - Sport: soccer - Teams: Hertha Berlin vs Schalke 04 - Score: 2 - 2 - Prediction: Both to Score - YES (1.87) - Outcome: WIN - Profit: 87
Date: 25. Jan 19:00 - Sport: soccer - Teams: Ajaccio vs Metz - Score: 0 - 0 - Prediction: Home or Draw (1.76) - Outcome: WIN - Profit: 76
Date: 23. Jan 19:45 - Sport: soccer - Teams: Feyenoord vs Fortuna Sittard - Score: 4 - 1 - Prediction: Both to Score - YES (1.8) - Outcome: WIN - Profit: 80
Date: 23. Jan 19:30 - Sport: soccer - Teams: Reggina vs Viterbese Castrense - Score: 3 - 1 - Prediction: Both to score - NO (1.83) - Outcome: LOST - Profit: -100
Date: 23. Jan 13:30 - Sport: soccer - Teams: FC Rieti vs Virtus Francavilla - Score: 0 - 2 - Prediction: Under 2,5 goals (1.6) - Outcome: WIN - Profit: 60
Date: 22. Jan 19:45 - Sport: soccer - Teams: Bury vs Oxford United - Score: 5 - 2 - Prediction: Draw or Away (1.61) - Outcome: LOST - Profit: -100
Date: 22. Jan 19:45 - Sport: soccer - Teams: Leamington vs Stockport County - Score: 0 - 1 - Prediction: Over 2,5 goals (1.73) - Outcome: LOST - Profit: -100
Date: 22. Jan 18:30 - Sport: soccer - Teams: Bnei Sakhnin vs Maccabi Haifa - Score: 1 - 0 - Prediction: Away win (1.65) - Outcome: LOST - Profit: -100
Date: 21. Jan 19:00 - Sport: soccer - Teams: Jong PSV vs MVV Maastricht - Score: 2 - 2 - Prediction: Draw or Away (1.97) - Outcome: WIN - Profit: 97
Date: 21. Jan 18:00 - Sport: soccer - Teams: Ashdod FC vs Hapoel Tel Aviv - Score: 1 - 1 - Prediction: Under 2,5 goals (1.6) - Outcome: WIN - Profit: 60
Date: 20. Jan 17:30 - Sport: soccer - Teams: Rayo Vallecano vs Real Sociedad - Score: 2 - 2 - Prediction: Over 2,5 goals (1.9) - Outcome: WIN - Profit: 89
Date: 20. Jan 13:30 - Sport: soccer - Teams: Trapani vs Vibonese Calcio - Score: 2 - 1 - Prediction: Under 2,5 goals (1.59) - Outcome: LOST - Profit: -100
Date: 20. Jan 13:00 - Sport: soccer - Teams: OFI Crete vs Panaitolikos - Score: 3 - 0 - Prediction: Under 2,5 goals (1.62) - Outcome: LOST - Profit: -100
Date: 19. Jan 19:30 - Sport: soccer - Teams: Zulte-Waregem vs Royal Antwerp - Score: 0 - 2 - Prediction: Over 2,5 goals (1.78) - Outcome: LOST - Profit: -100
Date: 19. Jan 15:00 - Sport: soccer - Teams: Almeria vs Cadiz - Score: 0 - 0 - Prediction: Both to score - NO (1.69) - Outcome: WIN - Profit: 69
Date: 19. Jan 15:00 - Sport: soccer - Teams: East Fife vs Greenock Morton - Score: 2 - 1 - Prediction: Home or Draw (1.72) - Outcome: WIN - Profit: 72
Date: 19. Jan 14:00 - Sport: soccer - Teams: Carpi vs Foggia - Score: 0 - 2 - Prediction: Under 2,5 goals (1.77) - Outcome: WIN - Profit: 77
Date: 18. Jan 19:00 - Sport: soccer - Teams: JS Saoura vs Al Ahly - Score: 1 - 1 - Prediction: Away win (2.5) - Outcome: LOST - Profit: -100
Date: 18. Jan 19:00 - Sport: soccer - Teams: Rodez Aveyron vs Entente Sannois St. Gratien - Score: 1 - 0 - Prediction: Under 2,5 goals (1.62) - Outcome: WIN - Profit: 62
Date: 18. Jan 19:00 - Sport: soccer - Teams: Sochaux vs Ajaccio - Score: 0 - 0 - Prediction: Both to score - NO (1.83) - Outcome: WIN - Profit: 83
Date: 17. Jan 18:00 - Sport: soccer - Teams: Portugal (w) vs Ukraine (w) - Score: 1 - 1 - Prediction: Both to Score - YES (1.92) - Outcome: WIN - Profit: 92
Date: 17. Jan 16:00 - Sport: soccer - Teams: Lebanon vs Korea DPR - Score: 4 - 1 - Prediction: Home win (1.6) - Outcome: WIN - Profit: 60
Date: 16. Jan 17:30 - Sport: soccer - Teams: Cape Town City vs Free State Stars - Score: 5 - 0 - Prediction: Both to score - NO (1.73) - Outcome: WIN - Profit: 73
Date: 16. Jan 13:00 - Sport: soccer - Teams: Mbao vs Singida United - Score: 1 - 0 - Prediction: Home win (1.86) - Outcome: WIN - Profit: 86
Date: 16. Jan 09:30 - Sport: soccer - Teams: Hradec Kralove vs Graffin Vlasim - Score: 1 - 2 - Prediction: Draw or Away (1.96) - Outcome: WIN - Profit: 96
Date: 15. Jan 20:00 - Sport: soccer - Teams: Stoke City vs Shrewsbury Town - Score: 2 - 3 - Prediction: Over 2,5 goals (1.96) - Outcome: WIN - Profit: 96
Date: 15. Jan 19:45 - Sport: soccer - Teams: Blackburn Rovers vs Newcastle United - Score: 2 - 2 - Prediction: Under 2,5 goals (1.7) - Outcome: LOST - Profit: -100
Date: 15. Jan 19:45 - Sport: soccer - Teams: Luton Town vs Sheffield Wednesday - Score: 0 - 1 - Prediction: Draw or Away (1.66) - Outcome: WIN - Profit: 65
Date: 14. Jan 20:15 - Sport: soccer - Teams: Famalicao vs Estoril - Score: 2 - 0 - Prediction: Under 2,5 goals (1.64) - Outcome: WIN - Profit: 63
Date: 14. Jan 20:00 - Sport: soccer - Teams: Roma vs Virtus Entella - Score: 4 - 0 - Prediction: Both to score - NO (1.73) - Outcome: WIN - Profit: 73
Date: 14. Jan 19:45 - Sport: soccer - Teams: Red Star vs Lens - Score: 1 - 0 - Prediction: Both to score - NO (1.8) - Outcome: WIN - Profit: 80
Date: 13. Jan 16:30 - Sport: soccer - Teams: Mirandes vs Izarra - Score: 1 - 1 - Prediction: Under 2,5 goals (1.8) - Outcome: WIN - Profit: 80
Date: 13. Jan 15:30 - Sport: soccer - Teams: Real Murcia vs Atletico Sanluqueno - Score: 1 - 0 - Prediction: Home win (1.62) - Outcome: WIN - Profit: 62
Date: 13. Jan 14:00 - Sport: soccer - Teams: Apollon Limassol vs Omonia Nicosia - Score: 2 - 1 - Prediction: Both to Score - YES (1.85) - Outcome: WIN - Profit: 85
Date: 13. Jan 11:00 - Sport: soccer - Teams: SS Reyes vs Real Valladolid II - Score: 1 - 0 - Prediction: Home win (1.95) - Outcome: WIN - Profit: 95
Date: 12. Jan 16:00 - Sport: soccer - Teams: Leioa vs Sporting Gijon B - Score: 2 - 0 - Prediction: Both to Score - YES (1.93) - Outcome: LOST - Profit: -100
Date: 12. Jan 15:00 - Sport: soccer - Teams: Burgess Hill Town vs Bognor Regis Town - Score: 0 - 2 - Prediction: Home or Draw (1.57) - Outcome: LOST - Profit: -100
Date: 12. Jan 15:00 - Sport: soccer - Teams: Chorley vs Altrincham - Score: 4 - 1 - Prediction: Both to Score - YES (1.57) - Outcome: WIN - Profit: 57
Date: 12. Jan 15:00 - Sport: soccer - Teams: Exeter City vs Morecambe - Score: 0 - 0 - Prediction: Under 2,5 goals (1.96) - Outcome: WIN - Profit: 96
Date: 12. Jan 15:00 - Sport: soccer - Teams: Wrexham vs Leyton Orient - Score: 0 - 1 - Prediction: Both to Score - YES (1.86) - Outcome: LOST - Profit: -100
Date: 11. Jan 19:30 - Sport: soccer - Teams: Lommel vs Westerlo - Score: 2 - 1 - Prediction: Under 2,5 goals (1.76) - Outcome: LOST - Profit: -100
Date: 11. Jan 19:00 - Sport: soccer - Teams: Gazelec Ajaccio vs Auxerre - Score: 0 - 4 - Prediction: Home or Draw (1.61) - Outcome: LOST - Profit: -100
Date: 11. Jan 19:00 - Sport: soccer - Teams: Lorient vs Chateauroux - Score: 2 - 1 - Prediction: Under 2,5 goals (1.69) - Outcome: LOST - Profit: -100
Date: 10. Jan 19:30 - Sport: soccer - Teams: Real Betis vs Real Sociedad - Score: 0 - 0 - Prediction: Both to score - NO (1.94) - Outcome: WIN - Profit: 94
Date: 10. Jan 13:30 - Sport: soccer - Teams: Jordan vs Syria - Score: 2 - 0 - Prediction: Home or Draw (1.66) - Outcome: WIN - Profit: 65
Date: 10. Jan 11:00 - Sport: soccer - Teams: Bahrain vs Thailand - Score: 0 - 1 - Prediction: Under 2,5 goals (1.6) - Outcome: WIN - Profit: 60
Date: 9. Jan 20:05 - Sport: soccer - Teams: Bordeaux vs Le Havre - Score: 1 - 0 - Prediction: Both to score - NO (1.92) - Outcome: WIN - Profit: 92
Date: 9. Jan 17:00 - Sport: soccer - Teams: AEK Larnaca vs Apollon Limassol - Score: 0 - 0 - Prediction: Over 2,5 goals (1.85) - Outcome: LOST - Profit: -100
Date: 9. Jan 16:00 - Sport: soccer - Teams: Qatar vs Lebanon - Score: 2 - 0 - Prediction: Home win (1.73) - Outcome: WIN - Profit: 73
Date: 9. Jan 11:00 - Sport: soccer - Teams: Perth Glory vs Sydney FC - Score: 3 - 1 - Prediction: Over 2,5 goals (1.89) - Outcome: WIN - Profit: 88
Date: 8. Jan 19:45 - Sport: soccer - Teams: Accrington Stanley vs Bury - Score: 2 - 4 - Prediction: Both to Score - YES (1.61) - Outcome: WIN - Profit: 61
Date: 8. Jan 19:45 - Sport: soccer - Teams: Bromley vs Wrexham - Score: 2 - 0 - Prediction: Under 2,5 goals (1.93) - Outcome: WIN - Profit: 93
Date: 8. Jan 15:00 - Sport: soccer - Teams: Smouha SC vs Al Ahly - Score: 0 - 1 - Prediction: Home or Draw (1.72) - Outcome: LOST - Profit: -100
Date: 8. Jan 12:00 - Sport: soccer - Teams: Tatran Presov vs Poprad FK - Score: 3 - 0 - Prediction: Home or Draw (2.83) - Outcome: WIN - Profit: 183
Date: 7. Jan 20:00 - Sport: soccer - Teams: Celta de Vigo vs Athletic Bilbao - Score: 1 - 2 - Prediction: Draw or Away (1.64) - Outcome: WIN - Profit: 63
Date: 7. Jan 11:00 - Sport: soccer - Teams: Burgos CF vs Fuenlabrada - Score: 1 - 0 - Prediction: Under 2,5 goals (1.57) - Outcome: WIN - Profit: 57
Date: 6. Jan 16:00 - Sport: soccer - Teams: Nea Salamis vs Apollon Limassol - Score: 0 - 3 - Prediction: Away win (1.77) - Outcome: WIN - Profit: 77
Date: 6. Jan 16:00 - Sport: soccer - Teams: Syria vs Palestine - Score: 0 - 0 - Prediction: Home win (1.75) - Outcome: LOST - Profit: -100
Date: 6. Jan 14:00 - Sport: soccer - Teams: Millwall vs Hull City - Score: 2 - 1 - Prediction: Both to Score - YES (1.73) - Outcome: WIN - Profit: 73
Date: 6. Jan 11:00 - Sport: soccer - Teams: Espanyol II vs Valencia II - Score: 1 - 3 - Prediction: Both to Score - YES (1.88) - Outcome: WIN - Profit: 87
Date: 5. Jan 18:15 - Sport: soccer - Teams: Highlands Park vs Orlando Pirates - Score: 2 - 2 - Prediction: Under 2,5 goals (1.58) - Outcome: LOST - Profit: -100
Date: 5. Jan 15:00 - Sport: soccer - Teams: Enfield Town vs Lewes - Score: 1 - 1 - Prediction: Draw or Away (1.72) - Outcome: WIN - Profit: 72
Date: 5. Jan 15:00 - Sport: soccer - Teams: North Ferriby United vs Marine - Score: 0 - 2 - Prediction: Over 2,5 goals (1.57) - Outcome: LOST - Profit: -100
Date: 5. Jan 12:30 - Sport: soccer - Teams: ASIL vs EN THOI Lakatamia - Score: 4 - 1 - Prediction: Home win (1.74) - Outcome: WIN - Profit: 74
Date: 4. Jan 18:00 - Sport: soccer - Teams: Levante vs Girona - Score: 2 - 2 - Prediction: Both to Score - YES (1.64) - Outcome: WIN - Profit: 63
Date: 4. Jan 13:00 - Sport: soccer - Teams: Beitar Tel Aviv vs Hapoel Ashkelon - Score: 2 - 0 - Prediction: Under 2,5 goals (1.57) - Outcome: WIN - Profit: 57
Date: 4. Jan 13:00 - Sport: soccer - Teams: Hapoel Nazareth Illit vs Hapoel Iksal Imad - Score: 1 - 0 - Prediction: Under 2,5 goals (1.65) - Outcome: WIN - Profit: 64
Date: 3. Jan 16:00 - Sport: soccer - Teams: Chaves vs Feirense - Score: 0 - 0 - Prediction: Both to score - NO (1.77) - Outcome: WIN - Profit: 77
Date: 2. Jan 20:00 - Sport: soccer - Teams: Newcastle United vs Manchester United - Score: 0 - 2 - Prediction: Away win (1.66) - Outcome: WIN - Profit: 65
Date: 2. Jan 18:00 - Sport: soccer - Teams: Boavista vs Vitoria Setubal - Score: 1 - 0 - Prediction: Draw or Away (1.57) - Outcome: LOST - Profit: -100
Date: 2. Jan 18:00 - Sport: soccer - Teams: Sporting Braga vs Maritimo - Score: 2 - 0 - Prediction: Both to score - NO (1.62) - Outcome: WIN - Profit: 62
Date: 2. Jan 16:00 - Sport: soccer - Teams: Al Mokawloon Al Arab vs Ismaily - Score: 1 - 2 - Prediction: Both to Score - YES (1.95) - Outcome: WIN - Profit: 95
Date: 1. Jan 15:00 - Sport: soccer - Teams: Derby County vs Middlesbrough - Score: 1 - 1 - Prediction: Draw or Away (1.66) - Outcome: WIN - Profit: 65
Date: 1. Jan 15:00 - Sport: soccer - Teams: Luton Town vs Barnsley - Score: 0 - 0 - Prediction: Draw or Away (1.78) - Outcome: WIN - Profit: 78
Date: 31. Dec 11:00 - Sport: soccer - Teams: Philippines vs Vietnam - Score: 0 - 0 - Prediction: Under 2,5 goals (1.77) - Outcome: WIN - Profit: 77
Date: 31. Dec 08:00 - Sport: soccer - Teams: Central Coast Mariners vs Perth Glory - Score: 1 - 4 - Prediction: Both to Score - YES (1.73) - Outcome: WIN - Profit: 73
Date: 30. Dec 17:00 - Sport: soccer - Teams: Belenenses vs FC Porto - Score: 1 - 2 - Prediction: Over 2,5 goals (1.73) - Outcome: WIN - Profit: 73
Date: 30. Dec 17:00 - Sport: soccer - Teams: Livorno vs Calcio Padova - Score: 1 - 1 - Prediction: Both to score - NO (1.84) - Outcome: LOST - Profit: -100
Date: 30. Dec 13:30 - Sport: soccer - Teams: Piacenza vs Pro Vercelli - Score: 0 - 1 - Prediction: Both to score - NO (1.8) - Outcome: WIN - Profit: 80
Date: 29. Dec 15:00 - Sport: soccer - Teams: Gillingham vs Doncaster Rovers - Score: 1 - 3 - Prediction: Both to Score - YES (1.66) - Outcome: WIN - Profit: 65
Date: 29. Dec 15:00 - Sport: soccer - Teams: Leiston vs Royston Town - Score: 1 - 1 - Prediction: Draw or Away (1.58) - Outcome: WIN - Profit: 58
Date: 29. Dec 15:00 - Sport: soccer - Teams: Sheffield United vs Blackburn Rovers - Score: 3 - 0 - Prediction: Both to Score - YES (1.88) - Outcome: LOST - Profit: -100
Date: 28. Dec 21:15 - Sport: soccer - Teams: Desportivo Aves vs Benfica - Score: 1 - 1 - Prediction: Away win (1.62) - Outcome: LOST - Profit: -100
Date: 28. Dec 21:15 - Sport: soccer - Teams: Rio Ave vs Pacos Ferreira - Score: 1 - 1 - Prediction: Under 2,5 goals (1.84) - Outcome: WIN - Profit: 84
Date: 28. Dec 19:00 - Sport: soccer - Teams: Vitoria Setubal vs Sporting Braga - Score: 0 - 4 - Prediction: Away win (1.77) - Outcome: WIN - Profit: 77
Date: 27. Dec 20:00 - Sport: soccer - Teams: Calcio Padova vs Benevento - Score: 0 - 1 - Prediction: Both to Score - YES (1.87) - Outcome: LOST - Profit: -100
Date: 27. Dec 20:00 - Sport: soccer - Teams: Hellas Verona vs Cittadella - Score: 4 - 0 - Prediction: Both to score - NO (1.95) - Outcome: WIN - Profit: 95
Date: 27. Dec 14:55 - Sport: soccer - Teams: Al Faisaly vs Al Feiha - Score: 1 - 0 - Prediction: Both to Score - YES (1.66) - Outcome: LOST - Profit: -100
Date: 26. Dec 15:00 - Sport: soccer - Teams: Tranmere Rovers vs Morecambe - Score: 3 - 1 - Prediction: Under 2,5 goals (1.93) - Outcome: LOST - Profit: -100
Date: 26. Dec 15:00 - Sport: soccer - Teams: Walsall vs Bristol Rovers - Score: 1 - 3 - Prediction: Under 2,5 goals (1.83) - Outcome: LOST - Profit: -100
Date: 25. Dec 17:00 - Sport: soccer - Teams: Hapoel Nazareth Illit vs Hapoel Nir Ramat HaSharon - Score: 0 - 0 - Prediction: Draw or Away (1.78) - Outcome: WIN - Profit: 78
Date: 24. Dec 17:00 - Sport: soccer - Teams: Antalyaspor vs Fenerbahce - Score: 0 - 0 - Prediction: Away win (1.87) - Outcome: LOST - Profit: -100
Date: 23. Dec 16:00 - Sport: soccer - Teams: Enosis Neon Paralimni vs Omonia Nicosia - Score: 1 - 1 - Prediction: Both to Score - YES (1.72) - Outcome: WIN - Profit: 72
Date: 23. Dec 13:30 - Sport: soccer - Teams: MVV Maastricht vs Cambuur Leeuwarden - Score: 1 - 2 - Prediction: Both to Score - YES (1.62) - Outcome: WIN - Profit: 62
