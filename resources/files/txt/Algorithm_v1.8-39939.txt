Algorithm: Algorithm_v1.8-39939
Algorithm info: Total predictions: 108
Total profit: 1933
Total wins: 77
Total lost: 31

Date: 28. Jan 19:45 - Sport: soccer - Teams: Cavese vs FC Rieti - Score: 1 - 1 - Prediction: Draw or Away (1.82) - Outcome: WIN - Profit: 82
Date: 28. Jan 19:45 - Sport: soccer - Teams: Troyes vs Lens - Score: 1 - 0 - Prediction: Both to score - NO (1.83) - Outcome: WIN - Profit: 83
Date: 28. Jan 17:00 - Sport: soccer - Teams: Rostov vs Zenit St. Petersburg - Score: 0 - 5 - Prediction: Home or Draw (1.77) - Outcome: LOST - Profit: -100
Date: 28. Jan 13:00 - Sport: soccer - Teams: Irodotos vs Panachaiki - Score: 0 - 1 - Prediction: Both to score - NO (1.87) - Outcome: WIN - Profit: 87
Date: 27. Jan 15:30 - Sport: soccer - Teams: Virtus Francavilla vs Sicula Leonzio - Score: 1 - 0 - Prediction: Both to score - NO (1.75) - Outcome: WIN - Profit: 75
Date: 26. Jan 18:45 - Sport: soccer - Teams: Almere City vs FC Twente - Score: 1 - 3 - Prediction: Home or Draw (1.79) - Outcome: LOST - Profit: -100
Date: 26. Jan 17:00 - Sport: soccer - Teams: Lugo vs Rayo Majadahonda - Score: 3 - 2 - Prediction: Under 2,5 goals (1.63) - Outcome: LOST - Profit: -100
Date: 26. Jan 17:00 - Sport: soccer - Teams: Panaitolikos vs Xanthi - Score: 0 - 0 - Prediction: Draw or Away (1.65) - Outcome: WIN - Profit: 64
Date: 26. Jan 15:00 - Sport: soccer - Teams: Harlow Town vs Lewes - Score: 2 - 1 - Prediction: Home or Draw (1.8) - Outcome: WIN - Profit: 80
Date: 26. Jan 13:45 - Sport: soccer - Teams: USM Alger U21 vs JS Saoura U21 - Score: 1 - 0 - Prediction: Under 2,5 goals (1.8) - Outcome: WIN - Profit: 80
Date: 25. Jan 15:00 - Sport: soccer - Teams: Al Masry Club vs Al Mokawloon Al Arab - Score: 1 - 0 - Prediction: Under 2,5 goals (1.62) - Outcome: WIN - Profit: 62
Date: 25. Jan 14:00 - Sport: soccer - Teams: Saida vs ASM Oran - Score: 1 - 0 - Prediction: Home win (1.55) - Outcome: WIN - Profit: 55
Date: 23. Jan 19:30 - Sport: soccer - Teams: Sicula Leonzio vs Trapani - Score: 0 - 0 - Prediction: Both to score - NO (1.8) - Outcome: WIN - Profit: 80
Date: 23. Jan 17:30 - Sport: soccer - Teams: Olympiakos Piraeus vs Xanthi - Score: 3 - 1 - Prediction: Under 2,5 goals (1.85) - Outcome: LOST - Profit: -100
Date: 23. Jan 17:30 - Sport: soccer - Teams: Robur Siena vs Pro Patria - Score: 1 - 0 - Prediction: Draw or Away (1.46) - Outcome: LOST - Profit: -100
Date: 23. Jan 14:50 - Sport: soccer - Teams: Burgan vs Al Tadhamon - Score: 9 - 0 - Prediction: Over 2,5 goals (1.4) - Outcome: WIN - Profit: 39
Date: 22. Jan 19:45 - Sport: soccer - Teams: AFC Wimbledon vs Fleetwood Town - Score: 0 - 3 - Prediction: Draw or Away (1.58) - Outcome: WIN - Profit: 58
Date: 22. Jan 19:45 - Sport: soccer - Teams: Dorking Wanderers vs Enfield Town - Score: 1 - 0 - Prediction: Draw or Away (1.75) - Outcome: LOST - Profit: -100
Date: 22. Jan 19:45 - Sport: soccer - Teams: Guiseley AFC vs Hereford FC - Score: 1 - 1 - Prediction: Draw or Away (1.81) - Outcome: WIN - Profit: 81
Date: 22. Jan 17:30 - Sport: soccer - Teams: Villefranche vs Vendee Les Herbiers - Score: 2 - 0 - Prediction: Home win (1.81) - Outcome: WIN - Profit: 81
Date: 21. Jan 19:30 - Sport: soccer - Teams: Juventus vs Chievo Verona - Score: 3 - 0 - Prediction: Over 2,5 goals (1.57) - Outcome: WIN - Profit: 57
Date: 21. Jan 18:00 - Sport: soccer - Teams: Ashdod FC vs Hapoel Tel Aviv - Score: 1 - 1 - Prediction: Under 2,5 goals (1.6) - Outcome: WIN - Profit: 60
Date: 21. Jan 14:00 - Sport: soccer - Teams: El Eulma U21 vs El Harrach U21 - Score: 1 - 1 - Prediction: Draw or Away (1.7) - Outcome: WIN - Profit: 70
Date: 20. Jan 19:00 - Sport: soccer - Teams: Extremadura UD vs Real Oviedo - Score: 0 - 2 - Prediction: Under 2,5 goals (1.58) - Outcome: WIN - Profit: 58
Date: 20. Jan 15:45 - Sport: soccer - Teams: Ajax vs SC Heerenveen - Score: 4 - 4 - Prediction: Both to score - NO (1.62) - Outcome: LOST - Profit: -100
Date: 20. Jan 15:00 - Sport: soccer - Teams: Hibernians FC vs Valletta FC - Score: 1 - 2 - Prediction: Both to Score - YES (1.67) - Outcome: WIN - Profit: 67
Date: 19. Jan 15:00 - Sport: soccer - Teams: Chorley vs Guiseley AFC - Score: 3 - 0 - Prediction: Both to Score - YES (1.8) - Outcome: LOST - Profit: -100
Date: 19. Jan 15:00 - Sport: soccer - Teams: Colchester United vs Mansfield Town - Score: 2 - 3 - Prediction: Home or Draw (1.59) - Outcome: LOST - Profit: -100
Date: 19. Jan 15:00 - Sport: soccer - Teams: Nottingham Forest vs Bristol City - Score: 0 - 1 - Prediction: Draw or Away (1.61) - Outcome: WIN - Profit: 61
Date: 19. Jan 15:00 - Sport: soccer - Teams: Rotherham United vs Brentford - Score: 2 - 4 - Prediction: Both to Score - YES (1.73) - Outcome: WIN - Profit: 73
Date: 19. Jan 15:00 - Sport: soccer - Teams: Watford vs Burnley - Score: 0 - 0 - Prediction: Over 2,5 goals (1.85) - Outcome: LOST - Profit: -100
Date: 18. Jan 19:00 - Sport: soccer - Teams: Go Ahead Eagles vs Almere City - Score: 2 - 1 - Prediction: Home win (1.64) - Outcome: WIN - Profit: 63
Date: 18. Jan 19:00 - Sport: soccer - Teams: Helmond Sport vs Cambuur Leeuwarden - Score: 0 - 1 - Prediction: Home or Draw (1.83) - Outcome: LOST - Profit: -100
Date: 18. Jan 19:00 - Sport: soccer - Teams: JS Saoura vs Al Ahly - Score: 1 - 1 - Prediction: Home or Draw (1.56) - Outcome: WIN - Profit: 56
Date: 17. Jan 19:30 - Sport: soccer - Teams: Espanyol vs Villarreal - Score: 3 - 1 - Prediction: Both to Score - YES (1.73) - Outcome: WIN - Profit: 73
Date: 17. Jan 17:00 - Sport: soccer - Teams: Maccabi Tel Aviv vs Hapoel Acre - Score: 4 - 0 - Prediction: Both to score - NO (1.85) - Outcome: WIN - Profit: 85
Date: 17. Jan 16:45 - Sport: soccer - Teams: MC Alger vs Belouizdad - Score: 1 - 1 - Prediction: Home win (1.6) - Outcome: LOST - Profit: -100
Date: 16. Jan 19:30 - Sport: soccer - Teams: Sevilla vs Athletic Bilbao - Score: 0 - 1 - Prediction: Over 2,5 goals (1.76) - Outcome: LOST - Profit: -100
Date: 16. Jan 09:30 - Sport: soccer - Teams: Nitra vs Znojmo - Score: 4 - 4 - Prediction: Draw or Away (1.9) - Outcome: WIN - Profit: 89
Date: 15. Jan 19:45 - Sport: soccer - Teams: Kettering Town vs Biggleswade Town - Score: 2 - 3 - Prediction: Over 2,5 goals (1.65) - Outcome: WIN - Profit: 64
Date: 15. Jan 19:45 - Sport: soccer - Teams: Poole Town vs Swindon Supermarine - Score: 2 - 5 - Prediction: Home win (1.87) - Outcome: LOST - Profit: -100
Date: 15. Jan 19:30 - Sport: soccer - Teams: Leixoes vs FC Porto - Score: 1 - 1 - Prediction: Both to score - NO (1.73) - Outcome: LOST - Profit: -100
Date: 15. Jan 18:30 - Sport: soccer - Teams: Real Valladolid vs Getafe - Score: 1 - 1 - Prediction: Home or Draw (1.52) - Outcome: WIN - Profit: 52
Date: 14. Jan 20:15 - Sport: soccer - Teams: Famalicao vs Estoril - Score: 2 - 0 - Prediction: Draw or Away (1.67) - Outcome: LOST - Profit: -100
Date: 14. Jan 20:00 - Sport: soccer - Teams: Roma vs Virtus Entella - Score: 4 - 0 - Prediction: Both to score - NO (1.73) - Outcome: WIN - Profit: 73
Date: 14. Jan 19:45 - Sport: soccer - Teams: Red Star vs Lens - Score: 1 - 0 - Prediction: Under 2,5 goals (1.69) - Outcome: WIN - Profit: 69
Date: 14. Jan 17:00 - Sport: soccer - Teams: Alki Oroklini vs AEL Limassol - Score: 1 - 3 - Prediction: Both to score - NO (1.8) - Outcome: LOST - Profit: -100
Date: 13. Jan 19:00 - Sport: soccer - Teams: OH Leuven vs Tubize - Score: 0 - 2 - Prediction: Over 2,5 goals (1.85) - Outcome: LOST - Profit: -100
Date: 13. Jan 13:00 - Sport: soccer - Teams: Iraklis vs Ergotelis - Score: 2 - 1 - Prediction: Both to score - NO (1.8) - Outcome: LOST - Profit: -100
Date: 13. Jan 12:00 - Sport: soccer - Teams: Fortuna Dusseldorf vs Bayern Munchen - Score: 7 - 8 - Prediction: Away win (1.6) - Outcome: WIN - Profit: 60
Date: 13. Jan 11:15 - Sport: soccer - Teams: Benfica B vs Sporting Braga B - Score: 2 - 3 - Prediction: Home win (1.61) - Outcome: LOST - Profit: -100
Date: 13. Jan 11:00 - Sport: soccer - Teams: Espanyol II vs Badalona - Score: 2 - 0 - Prediction: Under 2,5 goals (1.65) - Outcome: WIN - Profit: 64
Date: 12. Jan 15:00 - Sport: soccer - Teams: Bamber Bridge vs Hyde United FC - Score: 5 - 2 - Prediction: Over 2,5 goals (1.57) - Outcome: WIN - Profit: 57
Date: 12. Jan 15:00 - Sport: soccer - Teams: Brechin City vs Dumbarton - Score: 1 - 0 - Prediction: Home or Draw (1.67) - Outcome: WIN - Profit: 67
Date: 12. Jan 15:00 - Sport: soccer - Teams: Farsley Celtic vs Grantham - Score: 1 - 1 - Prediction: Both to Score - YES (1.6) - Outcome: WIN - Profit: 60
Date: 11. Jan 20:00 - Sport: soccer - Teams: Rayo Vallecano vs Celta de Vigo - Score: 4 - 2 - Prediction: Both to Score - YES (1.91) - Outcome: WIN - Profit: 90
Date: 11. Jan 19:00 - Sport: soccer - Teams: Desportivo Aves vs Feirense - Score: 1 - 1 - Prediction: Draw or Away (1.83) - Outcome: WIN - Profit: 83
Date: 11. Jan 13:00 - Sport: soccer - Teams: Hapoel Katamon vs Hapoel Akko - Score: 0 - 0 - Prediction: Both to score - NO (1.62) - Outcome: WIN - Profit: 62
Date: 10. Jan 19:30 - Sport: soccer - Teams: Real Betis vs Real Sociedad - Score: 0 - 0 - Prediction: Under 2,5 goals (1.82) - Outcome: WIN - Profit: 82
Date: 10. Jan 16:50 - Sport: soccer - Teams: Al Qadisiya vs Al Ittihad Jeddah - Score: 0 - 1 - Prediction: Home win (1.62) - Outcome: LOST - Profit: -100
Date: 10. Jan 13:30 - Sport: soccer - Teams: Jordan vs Syria - Score: 2 - 0 - Prediction: Home or Away (1.47) - Outcome: WIN - Profit: 47
Date: 10. Jan 13:30 - Sport: soccer - Teams: Tooro United vs Bright Stars - Score: 2 - 0 - Prediction: Both to score - NO (1.53) - Outcome: WIN - Profit: 53
Date: 9. Jan 17:30 - Sport: soccer - Teams: Bidvest Wits vs Kaizer Chiefs - Score: 0 - 2 - Prediction: Under 2,5 goals (1.54) - Outcome: WIN - Profit: 54
Date: 9. Jan 17:30 - Sport: soccer - Teams: Maritzburg United vs Free State Stars - Score: 1 - 2 - Prediction: Draw or Away (1.49) - Outcome: WIN - Profit: 49
Date: 9. Jan 16:00 - Sport: soccer - Teams: Qatar vs Lebanon - Score: 2 - 0 - Prediction: Under 2,5 goals (1.58) - Outcome: WIN - Profit: 58
Date: 9. Jan 08:50 - Sport: soccer - Teams: Adelaide United vs Melbourne Victory - Score: 2 - 0 - Prediction: Home or Draw (1.51) - Outcome: WIN - Profit: 51
Date: 8. Jan 20:00 - Sport: soccer - Teams: Lyon vs Strasbourg - Score: 1 - 2 - Prediction: Over 2,5 goals (1.71) - Outcome: WIN - Profit: 71
Date: 8. Jan 19:45 - Sport: soccer - Teams: Accrington Stanley vs Bury - Score: 2 - 4 - Prediction: Both to Score - YES (1.61) - Outcome: WIN - Profit: 61
Date: 8. Jan 19:45 - Sport: soccer - Teams: Cheltenham Town vs Oxford United - Score: 1 - 1 - Prediction: Both to Score - YES (1.57) - Outcome: WIN - Profit: 57
Date: 8. Jan 18:00 - Sport: soccer - Teams: Amiens SC vs Angers - Score: 0 - 0 - Prediction: Draw or Away (1.53) - Outcome: WIN - Profit: 53
Date: 8. Jan 17:30 - Sport: soccer - Teams: Orlando Pirates vs Chippa United - Score: 4 - 2 - Prediction: Home win (1.53) - Outcome: WIN - Profit: 53
Date: 7. Jan 20:00 - Sport: soccer - Teams: Celta de Vigo vs Athletic Bilbao - Score: 1 - 2 - Prediction: Draw or Away (1.64) - Outcome: WIN - Profit: 63
Date: 7. Jan 20:00 - Sport: soccer - Teams: Rayo Majadahonda vs Las Palmas - Score: 0 - 0 - Prediction: Home or Draw (1.47) - Outcome: WIN - Profit: 47
Date: 7. Jan 16:00 - Sport: soccer - Teams: San Fernando vs Almeria B - Score: 1 - 0 - Prediction: Under 2,5 goals (1.62) - Outcome: WIN - Profit: 62
Date: 7. Jan 11:00 - Sport: soccer - Teams: Burgos CF vs Fuenlabrada - Score: 1 - 0 - Prediction: Under 2,5 goals (1.57) - Outcome: WIN - Profit: 57
Date: 6. Jan 17:00 - Sport: soccer - Teams: Sabadell vs Olot - Score: 0 - 0 - Prediction: Under 2,5 goals (1.6) - Outcome: WIN - Profit: 60
Date: 6. Jan 14:00 - Sport: soccer - Teams: Queens Park Rangers vs Leeds United - Score: 2 - 1 - Prediction: Home or Draw (1.55) - Outcome: WIN - Profit: 55
Date: 6. Jan 13:30 - Sport: soccer - Teams: Baroka FC vs Black Leopards - Score: 1 - 2 - Prediction: Draw or Away (1.58) - Outcome: WIN - Profit: 58
Date: 5. Jan 15:00 - Sport: soccer - Teams: Banbury United vs Barwell - Score: 6 - 1 - Prediction: Both to Score - YES (1.6) - Outcome: WIN - Profit: 60
Date: 5. Jan 15:00 - Sport: soccer - Teams: Curzon Ashton vs York City - Score: 1 - 0 - Prediction: Draw or Away (1.62) - Outcome: LOST - Profit: -100
Date: 5. Jan 15:00 - Sport: soccer - Teams: Leyton Orient vs Salford City - Score: 0 - 3 - Prediction: Draw or Away (1.65) - Outcome: WIN - Profit: 64
Date: 5. Jan 15:00 - Sport: soccer - Teams: Telford United vs Spennymoor Town - Score: 2 - 1 - Prediction: Draw or Away (1.62) - Outcome: LOST - Profit: -100
Date: 4. Jan 19:55 - Sport: soccer - Teams: Nantes vs Chateauroux - Score: 4 - 1 - Prediction: Home win (1.51) - Outcome: WIN - Profit: 51
Date: 4. Jan 14:00 - Sport: soccer - Teams: Bordj Bou Arreridj vs MC Oran - Score: 3 - 1 - Prediction: Home or Draw (1.54) - Outcome: WIN - Profit: 54
Date: 4. Jan 14:00 - Sport: soccer - Teams: NA Hussein Dey vs Constantine - Score: 0 - 1 - Prediction: Draw or Away (1.68) - Outcome: WIN - Profit: 68
Date: 3. Jan 20:30 - Sport: soccer - Teams: Villarreal vs Real Madrid - Score: 2 - 2 - Prediction: Both to Score - YES (1.6) - Outcome: WIN - Profit: 60
Date: 3. Jan 18:00 - Sport: soccer - Teams: Sporting CP vs Belenenses - Score: 2 - 1 - Prediction: Over 2,5 goals (1.61) - Outcome: WIN - Profit: 61
Date: 2. Jan 17:00 - Sport: soccer - Teams: Pacos Ferreira vs Arouca - Score: 1 - 0 - Prediction: Both to score - NO (1.7) - Outcome: WIN - Profit: 70
Date: 2. Jan 16:00 - Sport: soccer - Teams: Santa Clara vs Tondela - Score: 1 - 2 - Prediction: Under 2,5 goals (1.76) - Outcome: LOST - Profit: -100
Date: 1. Jan 15:00 - Sport: soccer - Teams: Chester FC vs Southport - Score: 0 - 0 - Prediction: Both to Score - YES (1.72) - Outcome: LOST - Profit: -100
Date: 1. Jan 15:00 - Sport: soccer - Teams: Nottingham Forest vs Leeds United - Score: 4 - 2 - Prediction: Home or Draw (1.65) - Outcome: WIN - Profit: 64
Date: 1. Jan 15:00 - Sport: soccer - Teams: Stevenage vs Newport County - Score: 1 - 0 - Prediction: Draw or Away (1.54) - Outcome: LOST - Profit: -100
Date: 1. Jan 15:00 - Sport: soccer - Teams: Swindon Town vs Exeter City - Score: 0 - 2 - Prediction: Draw or Away (1.63) - Outcome: WIN - Profit: 62
Date: 31. Dec 11:00 - Sport: soccer - Teams: Philippines vs Vietnam - Score: 0 - 0 - Prediction: Under 2,5 goals (1.77) - Outcome: WIN - Profit: 77
Date: 31. Dec 08:00 - Sport: soccer - Teams: Central Coast Mariners vs Perth Glory - Score: 1 - 4 - Prediction: Away win (1.8) - Outcome: WIN - Profit: 80
Date: 30. Dec 22:30 - Sport: soccer - Teams: Newcastle Jets (w) vs Adelaide United (w) - Score: 2 - 3 - Prediction: Draw or Away (1.69) - Outcome: WIN - Profit: 69
Date: 30. Dec 17:00 - Sport: soccer - Teams: Belenenses vs FC Porto - Score: 1 - 2 - Prediction: Both to score - NO (1.78) - Outcome: LOST - Profit: -100
Date: 30. Dec 13:30 - Sport: soccer - Teams: Arezzo vs Lucchese - Score: 0 - 0 - Prediction: Both to score - NO (1.83) - Outcome: WIN - Profit: 83
Date: 30. Dec 11:30 - Sport: soccer - Teams: Virtus Entella vs ASDC Gozzano - Score: 0 - 0 - Prediction: Home win (1.5) - Outcome: LOST - Profit: -100
Date: 29. Dec 15:00 - Sport: soccer - Teams: Blyth Spartans vs York City - Score: 2 - 1 - Prediction: Over 2,5 goals (1.63) - Outcome: WIN - Profit: 62
Date: 29. Dec 15:00 - Sport: soccer - Teams: Farnborough vs Staines Town - Score: 1 - 0 - Prediction: Home win (1.63) - Outcome: WIN - Profit: 62
Date: 29. Dec 15:00 - Sport: soccer - Teams: Middlesbrough vs Ipswich Town - Score: 2 - 0 - Prediction: Both to score - NO (1.55) - Outcome: WIN - Profit: 55
Date: 28. Dec 11:30 - Sport: soccer - Teams: East Bengal vs Real Kashmir - Score: 1 - 1 - Prediction: Under 2,5 goals (1.68) - Outcome: WIN - Profit: 68
Date: 28. Dec 10:30 - Sport: soccer - Teams: Hapoel Migdal Haemek vs Hapoel Jerusalem - Score: 1 - 0 - Prediction: Draw or Away (1.76) - Outcome: LOST - Profit: -100
Date: 27. Dec 20:00 - Sport: soccer - Teams: Crotone vs Spezia - Score: 0 - 3 - Prediction: Draw or Away (1.69) - Outcome: WIN - Profit: 69
Date: 27. Dec 19:30 - Sport: soccer - Teams: Anderlecht vs RS Waasland-Beveren - Score: 3 - 0 - Prediction: Both to Score - YES (1.73) - Outcome: LOST - Profit: -100
Date: 27. Dec 13:30 - Sport: soccer - Teams: Ternana vs Teramo - Score: 2 - 1 - Prediction: Home win (1.83) - Outcome: WIN - Profit: 83
Date: 26. Dec 15:00 - Sport: soccer - Teams: Sunderland vs Bradford City - Score: 1 - 0 - Prediction: Over 2,5 goals (1.79) - Outcome: LOST - Profit: -100
