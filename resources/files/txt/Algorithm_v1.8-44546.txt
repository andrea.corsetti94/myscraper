Algorithm: Algorithm_v1.8-44546
Algorithm info: Total predictions: 107
Total profit: 2318
Total wins: 77
Total lost: 30

Date: 28. Jan 19:00 - Sport: soccer - Teams: Penafiel vs Famalicao - Score: 1 - 2 - Prediction: Under 2,5 goals (1.5) - Outcome: LOST - Profit: -100
Date: 28. Jan 19:00 - Sport: soccer - Teams: Tondela vs Desportivo Aves - Score: 0 - 2 - Prediction: Under 2,5 goals (1.8) - Outcome: WIN - Profit: 80
Date: 28. Jan 17:00 - Sport: soccer - Teams: APOEL FC vs Alki Oroklini - Score: 0 - 0 - Prediction: Over 2,5 goals (1.45) - Outcome: LOST - Profit: -100
Date: 28. Jan 17:00 - Sport: soccer - Teams: Maritimo vs Rio Ave - Score: 0 - 2 - Prediction: Draw or Away (1.62) - Outcome: WIN - Profit: 62
Date: 27. Jan 17:00 - Sport: soccer - Teams: Anderlecht vs Eupen - Score: 2 - 1 - Prediction: Over 2,5 goals (1.7) - Outcome: WIN - Profit: 70
Date: 27. Jan 16:00 - Sport: soccer - Teams: Crystal Palace vs Tottenham - Score: 2 - 0 - Prediction: Home or Draw (1.56) - Outcome: WIN - Profit: 56
Date: 27. Jan 14:00 - Sport: soccer - Teams: Venezia vs Calcio Padova - Score: 2 - 1 - Prediction: Draw or Away (1.84) - Outcome: LOST - Profit: -100
Date: 26. Jan 19:00 - Sport: soccer - Teams: Guingamp vs Reims - Score: 0 - 1 - Prediction: Draw or Away (1.61) - Outcome: WIN - Profit: 61
Date: 26. Jan 17:30 - Sport: soccer - Teams: Leganes vs Eibar - Score: 2 - 2 - Prediction: Draw or Away (1.54) - Outcome: WIN - Profit: 54
Date: 26. Jan 16:00 - Sport: soccer - Teams: Westerlo vs St. Gilloise - Score: 2 - 0 - Prediction: Draw or Away (1.46) - Outcome: LOST - Profit: -100
Date: 26. Jan 14:00 - Sport: soccer - Teams: Orleans vs Niort - Score: 1 - 0 - Prediction: Under 2,5 goals (1.61) - Outcome: WIN - Profit: 61
Date: 25. Jan 19:45 - Sport: soccer - Teams: Ayr United vs Dundee United - Score: 1 - 0 - Prediction: Draw or Away (1.54) - Outcome: LOST - Profit: -100
Date: 25. Jan 15:00 - Sport: soccer - Teams: Al Masry Club vs Al Mokawloon Al Arab - Score: 1 - 0 - Prediction: Home or Draw (1.6) - Outcome: WIN - Profit: 60
Date: 23. Jan 23:30 - Sport: soccer - Teams: Corinthians U20 vs Vasco da Gama U20 - Score: 2 - 2 - Prediction: Both to Score - YES (1.71) - Outcome: WIN - Profit: 71
Date: 23. Jan 23:00 - Sport: soccer - Teams: Chapecoense SC vs Criciuma SC - Score: 1 - 0 - Prediction: Under 2,5 goals (1.7) - Outcome: WIN - Profit: 70
Date: 23. Jan 13:30 - Sport: soccer - Teams: Carrarese vs Lucchese - Score: 3 - 1 - Prediction: Home win (1.59) - Outcome: WIN - Profit: 59
Date: 22. Jan 19:45 - Sport: soccer - Teams: AZ Alkmaar vs Vitesse - Score: 2 - 0 - Prediction: Both to Score - YES (1.54) - Outcome: LOST - Profit: -100
Date: 22. Jan 19:45 - Sport: soccer - Teams: Alvechurch vs Stratford Town - Score: 2 - 2 - Prediction: Both to Score - YES (1.54) - Outcome: WIN - Profit: 54
Date: 22. Jan 16:45 - Sport: soccer - Teams: MC Oran vs JS Saoura - Score: 1 - 1 - Prediction: Both to score - NO (1.73) - Outcome: LOST - Profit: -100
Date: 22. Jan 12:00 - Sport: soccer - Teams: Grasshoppers vs Vendsyssel FF - Score: 3 - 0 - Prediction: Over 2,5 goals (1.72) - Outcome: WIN - Profit: 72
Date: 22. Jan 12:00 - Sport: soccer - Teams: Podbrezova vs Dubnica - Score: 3 - 1 - Prediction: Over 2,5 goals (1.65) - Outcome: WIN - Profit: 64
Date: 21. Jan 19:45 - Sport: soccer - Teams: Paris FC vs Brest - Score: 0 - 1 - Prediction: Under 2,5 goals (1.6) - Outcome: WIN - Profit: 60
Date: 21. Jan 17:00 - Sport: soccer - Teams: Kasimpasa SK vs Rizespor - Score: 0 - 1 - Prediction: Over 2,5 goals (1.57) - Outcome: LOST - Profit: -100
Date: 21. Jan 09:00 - Sport: soccer - Teams: Gandzasar Kapan vs FK Armavir - Score: 1 - 3 - Prediction: Over 2,5 goals (1.73) - Outcome: WIN - Profit: 73
Date: 20. Jan 16:00 - Sport: soccer - Teams: Angers vs Nantes - Score: 1 - 0 - Prediction: Home or Draw (1.47) - Outcome: WIN - Profit: 47
Date: 20. Jan 15:30 - Sport: soccer - Teams: Virtus Francavilla vs Cavese - Score: 2 - 0 - Prediction: Draw or Away (1.56) - Outcome: LOST - Profit: -100
Date: 20. Jan 13:30 - Sport: soccer - Teams: SuperSport United vs Bloemfontein Celtic - Score: 2 - 1 - Prediction: Both to score - NO (1.73) - Outcome: LOST - Profit: -100
Date: 19. Jan 19:30 - Sport: soccer - Teams: France (w) vs USA (w) - Score: 3 - 1 - Prediction: Both to Score - YES (1.57) - Outcome: WIN - Profit: 57
Date: 19. Jan 15:00 - Sport: soccer - Teams: Stockport County vs York City - Score: 3 - 1 - Prediction: Over 2,5 goals (1.86) - Outcome: WIN - Profit: 86
Date: 19. Jan 13:55 - Sport: soccer - Teams: Lens vs Nancy - Score: 2 - 1 - Prediction: Both to score - NO (1.77) - Outcome: LOST - Profit: -100
Date: 19. Jan 12:30 - Sport: soccer - Teams: Wolverhampton vs Leicester City - Score: 4 - 3 - Prediction: Under 2,5 goals (1.64) - Outcome: LOST - Profit: -100
Date: 18. Jan 19:00 - Sport: soccer - Teams: Concarneau vs Drancy - Score: 0 - 0 - Prediction: Under 2,5 goals (1.48) - Outcome: WIN - Profit: 48
Date: 18. Jan 19:00 - Sport: soccer - Teams: Lorient vs Gazelec Ajaccio - Score: 0 - 1 - Prediction: Both to score - NO (1.7) - Outcome: WIN - Profit: 70
Date: 18. Jan 19:00 - Sport: soccer - Teams: Villefranche vs Chambly - Score: 0 - 0 - Prediction: Both to score - NO (1.73) - Outcome: WIN - Profit: 73
Date: 17. Jan 18:30 - Sport: soccer - Teams: Hapoel Tel Aviv vs Hapoel Eran Hadera - Score: 1 - 2 - Prediction: Draw or Away (1.79) - Outcome: WIN - Profit: 79
Date: 17. Jan 12:00 - Sport: soccer - Teams: Cyprus U19 vs Denmark U19 - Score: 1 - 1 - Prediction: Both to score - NO (1.73) - Outcome: LOST - Profit: -100
Date: 16. Jan 17:30 - Sport: soccer - Teams: Mamelodi Sundowns vs Maritzburg United - Score: 1 - 0 - Prediction: Under 2,5 goals (1.64) - Outcome: WIN - Profit: 63
Date: 16. Jan 16:30 - Sport: soccer - Teams: Nea Salamis vs AEK Larnaca - Score: 1 - 2 - Prediction: Draw or Away (1.54) - Outcome: WIN - Profit: 54
Date: 16. Jan 14:45 - Sport: soccer - Teams: Al Jahra vs Al Sulaibikhat - Score: 1 - 0 - Prediction: Draw or Away (1.67) - Outcome: LOST - Profit: -100
Date: 15. Jan 19:45 - Sport: soccer - Teams: Luton Town vs Sheffield Wednesday - Score: 0 - 1 - Prediction: Both to Score - YES (1.8) - Outcome: LOST - Profit: -100
Date: 15. Jan 18:00 - Sport: soccer - Teams: Desportivo Aves vs Sporting Braga - Score: 1 - 2 - Prediction: Both to score - NO (1.87) - Outcome: LOST - Profit: -100
Date: 15. Jan 18:00 - Sport: soccer - Teams: Le Havre vs Valenciennes - Score: 4 - 3 - Prediction: Under 2,5 goals (1.85) - Outcome: LOST - Profit: -100
Date: 14. Jan 20:00 - Sport: soccer - Teams: Roma vs Virtus Entella - Score: 4 - 0 - Prediction: Both to score - NO (1.73) - Outcome: WIN - Profit: 73
Date: 14. Jan 19:45 - Sport: soccer - Teams: Red Star vs Lens - Score: 1 - 0 - Prediction: Home or Draw (1.85) - Outcome: WIN - Profit: 85
Date: 13. Jan 16:00 - Sport: soccer - Teams: Pontevedra vs Unionistas de Salamanca - Score: 0 - 0 - Prediction: Draw or Away (1.83) - Outcome: WIN - Profit: 83
Date: 13. Jan 16:00 - Sport: soccer - Teams: Talavera CF vs FC Jumilla - Score: 2 - 0 - Prediction: Under 2,5 goals (1.53) - Outcome: WIN - Profit: 53
Date: 13. Jan 15:30 - Sport: soccer - Teams: Real Murcia vs Atletico Sanluqueno - Score: 1 - 0 - Prediction: Home win (1.62) - Outcome: WIN - Profit: 62
Date: 13. Jan 11:00 - Sport: soccer - Teams: Granada II vs FC Cartagena - Score: 0 - 2 - Prediction: Both to score - NO (1.75) - Outcome: WIN - Profit: 75
Date: 12. Jan 15:00 - Sport: soccer - Teams: Burnley vs Fulham - Score: 2 - 1 - Prediction: Under 2,5 goals (1.73) - Outcome: LOST - Profit: -100
Date: 12. Jan 15:00 - Sport: soccer - Teams: Pafos FC vs Nea Salamis - Score: 1 - 3 - Prediction: Both to Score - YES (1.75) - Outcome: WIN - Profit: 75
Date: 12. Jan 14:00 - Sport: soccer - Teams: USM Blida vs ASM Oran - Score: 1 - 0 - Prediction: Draw or Away (1.52) - Outcome: LOST - Profit: -100
Date: 11. Jan 19:00 - Sport: soccer - Teams: Clermont Foot vs Niort - Score: 3 - 2 - Prediction: Home win (1.76) - Outcome: WIN - Profit: 76
Date: 11. Jan 19:00 - Sport: soccer - Teams: Marignane Gignac vs Villefranche - Score: 1 - 1 - Prediction: Both to score - NO (1.55) - Outcome: LOST - Profit: -100
Date: 11. Jan 16:45 - Sport: soccer - Teams: Sweden vs Iceland - Score: 2 - 2 - Prediction: Draw or Away (1.88) - Outcome: WIN - Profit: 87
Date: 10. Jan 17:30 - Sport: soccer - Teams: Lamia vs Panathinaikos - Score: 1 - 0 - Prediction: Under 2,5 goals (1.57) - Outcome: WIN - Profit: 57
Date: 10. Jan 16:50 - Sport: soccer - Teams: Al Qadisiya vs Al Ittihad Jeddah - Score: 0 - 1 - Prediction: Home win (1.62) - Outcome: LOST - Profit: -100
Date: 9. Jan 19:30 - Sport: soccer - Teams: Getafe vs Real Valladolid - Score: 1 - 0 - Prediction: Under 2,5 goals (1.54) - Outcome: WIN - Profit: 54
Date: 9. Jan 17:30 - Sport: soccer - Teams: Bidvest Wits vs Kaizer Chiefs - Score: 0 - 2 - Prediction: Home win (1.81) - Outcome: LOST - Profit: -100
Date: 8. Jan 20:30 - Sport: soccer - Teams: Sporting Gijon vs Valencia - Score: 2 - 1 - Prediction: Both to Score - YES (1.9) - Outcome: WIN - Profit: 89
Date: 8. Jan 19:45 - Sport: soccer - Teams: Brightlingsea Regent vs Bognor Regis Town - Score: 3 - 0 - Prediction: Home or Draw (1.76) - Outcome: WIN - Profit: 76
Date: 8. Jan 15:15 - Sport: soccer - Teams: Xanthi vs Olympiakos Piraeus - Score: 0 - 0 - Prediction: Both to score - NO (1.62) - Outcome: WIN - Profit: 62
Date: 7. Jan 16:00 - Sport: soccer - Teams: CD Badajoz vs Atletico Sanluqueno - Score: 4 - 0 - Prediction: Under 2,5 goals (1.6) - Outcome: LOST - Profit: -100
Date: 7. Jan 16:00 - Sport: soccer - Teams: San Fernando vs Almeria B - Score: 1 - 0 - Prediction: Both to score - NO (1.67) - Outcome: WIN - Profit: 67
Date: 7. Jan 11:00 - Sport: soccer - Teams: Linense vs FC Cartagena - Score: 0 - 1 - Prediction: Both to score - NO (1.83) - Outcome: WIN - Profit: 83
Date: 6. Jan 16:00 - Sport: soccer - Teams: Talavera CF vs UCAM Murcia - Score: 0 - 0 - Prediction: Home or Draw (1.6) - Outcome: WIN - Profit: 60
Date: 6. Jan 15:30 - Sport: soccer - Teams: Rapido Bouzas vs Unionistas de Salamanca - Score: 4 - 1 - Prediction: Home or Draw (1.67) - Outcome: WIN - Profit: 67
Date: 6. Jan 11:15 - Sport: soccer - Teams: Famalicao vs Varzim - Score: 0 - 0 - Prediction: Both to score - NO (1.7) - Outcome: WIN - Profit: 70
Date: 5. Jan 15:00 - Sport: soccer - Teams: East Fife vs Airdrieonians - Score: 1 - 2 - Prediction: Both to Score - YES (1.65) - Outcome: WIN - Profit: 64
Date: 5. Jan 15:00 - Sport: soccer - Teams: East Thurrock vs Gloucester City - Score: 2 - 0 - Prediction: Both to Score - YES (1.66) - Outcome: LOST - Profit: -100
Date: 5. Jan 15:00 - Sport: soccer - Teams: Raith Rovers vs Stenhousemuir - Score: 5 - 1 - Prediction: Both to Score - YES (1.8) - Outcome: WIN - Profit: 80
Date: 5. Jan 15:00 - Sport: soccer - Teams: Staines Town vs Walton Casuals - Score: 1 - 4 - Prediction: Away win (1.76) - Outcome: WIN - Profit: 76
Date: 5. Jan 15:00 - Sport: soccer - Teams: Stratford Town vs Alvechurch - Score: 0 - 1 - Prediction: Under 2,5 goals (1.9) - Outcome: WIN - Profit: 89
Date: 5. Jan 14:30 - Sport: soccer - Teams: Llanelli Town vs Llandudno - Score: 2 - 1 - Prediction: Over 2,5 goals (1.53) - Outcome: WIN - Profit: 53
Date: 4. Jan 18:00 - Sport: soccer - Teams: Chippa United vs Bloemfontein Celtic - Score: 0 - 1 - Prediction: Under 2,5 goals (1.54) - Outcome: WIN - Profit: 54
Date: 4. Jan 13:00 - Sport: soccer - Teams: Hapoel Nazareth Illit vs Hapoel Iksal Imad - Score: 1 - 0 - Prediction: Home win (1.83) - Outcome: WIN - Profit: 83
Date: 4. Jan 06:20 - Sport: soccer - Teams: Sydney (w) vs Newcastle Jets (w) - Score: 3 - 1 - Prediction: Home win (1.8) - Outcome: WIN - Profit: 80
Date: 3. Jan 16:00 - Sport: soccer - Teams: Chaves vs Feirense - Score: 0 - 0 - Prediction: Under 2,5 goals (1.64) - Outcome: WIN - Profit: 63
Date: 3. Jan 09:30 - Sport: soccer - Teams: Perth Glory (w) vs Canberra (w) - Score: 1 - 0 - Prediction: Draw or Away (2.27) - Outcome: LOST - Profit: -100
Date: 2. Jan 18:00 - Sport: soccer - Teams: Boavista vs Vitoria Setubal - Score: 1 - 0 - Prediction: Under 2,5 goals (1.57) - Outcome: WIN - Profit: 57
Date: 2. Jan 15:00 - Sport: soccer - Teams: Vitoria Guimaraes B vs Famalicao - Score: 0 - 1 - Prediction: Under 2,5 goals (1.63) - Outcome: WIN - Profit: 62
Date: 2. Jan 12:00 - Sport: soccer - Teams: Western Stima vs Sofapaka - Score: 1 - 1 - Prediction: Draw or Away (1.73) - Outcome: WIN - Profit: 73
Date: 1. Jan 15:00 - Sport: soccer - Teams: Nottingham Forest vs Leeds United - Score: 4 - 2 - Prediction: Home or Draw (1.65) - Outcome: WIN - Profit: 64
Date: 1. Jan 15:00 - Sport: soccer - Teams: Stevenage vs Newport County - Score: 1 - 0 - Prediction: Draw or Away (1.54) - Outcome: LOST - Profit: -100
Date: 1. Jan 12:30 - Sport: soccer - Teams: Everton vs Leicester City - Score: 0 - 1 - Prediction: Under 2,5 goals (1.89) - Outcome: WIN - Profit: 88
Date: 31. Dec 11:00 - Sport: soccer - Teams: Philippines vs Vietnam - Score: 0 - 0 - Prediction: Under 2,5 goals (1.77) - Outcome: WIN - Profit: 77
Date: 31. Dec 08:00 - Sport: soccer - Teams: Central Coast Mariners vs Perth Glory - Score: 1 - 4 - Prediction: Both to Score - YES (1.73) - Outcome: WIN - Profit: 73
Date: 30. Dec 20:00 - Sport: soccer - Teams: Spezia vs Lecce - Score: 1 - 1 - Prediction: Under 2,5 goals (1.76) - Outcome: WIN - Profit: 76
Date: 30. Dec 15:00 - Sport: soccer - Teams: Benevento vs Brescia - Score: 1 - 1 - Prediction: Both to Score - YES (1.8) - Outcome: WIN - Profit: 80
Date: 30. Dec 12:45 - Sport: soccer - Teams: Ohod vs Al Raed - Score: 0 - 1 - Prediction: Under 2,5 goals (1.86) - Outcome: WIN - Profit: 86
Date: 29. Dec 15:00 - Sport: soccer - Teams: Beaconsfield Town vs Basingstoke Town - Score: 3 - 2 - Prediction: Over 2,5 goals (1.5) - Outcome: WIN - Profit: 50
Date: 29. Dec 15:00 - Sport: soccer - Teams: Birmingham City vs Brentford - Score: 0 - 0 - Prediction: Draw or Away (1.7) - Outcome: WIN - Profit: 70
Date: 29. Dec 15:00 - Sport: soccer - Teams: Darlington 1883 vs Ashton United FC - Score: 2 - 1 - Prediction: Both to Score - YES (1.55) - Outcome: WIN - Profit: 55
Date: 29. Dec 15:00 - Sport: soccer - Teams: Kilmarnock vs St. Mirren - Score: 2 - 1 - Prediction: Under 2,5 goals (1.72) - Outcome: LOST - Profit: -100
Date: 28. Dec 13:00 - Sport: soccer - Teams: Maccabi Ahi Nazareth vs Hapoel Iksal Imad - Score: 0 - 0 - Prediction: Draw or Away (1.62) - Outcome: WIN - Profit: 62
Date: 28. Dec 11:30 - Sport: soccer - Teams: East Bengal vs Real Kashmir - Score: 1 - 1 - Prediction: Under 2,5 goals (1.68) - Outcome: WIN - Profit: 68
Date: 28. Dec 11:00 - Sport: soccer - Teams: Hapoel Kfar Shalem vs Kafr Qasim - Score: 0 - 2 - Prediction: Away win (1.83) - Outcome: WIN - Profit: 83
Date: 28. Dec 06:20 - Sport: soccer - Teams: Melbourne Victory (w) vs Perth Glory (w) - Score: 2 - 1 - Prediction: Home or Draw (1.63) - Outcome: WIN - Profit: 62
Date: 27. Dec 20:00 - Sport: soccer - Teams: Calcio Padova vs Benevento - Score: 0 - 1 - Prediction: Home or Draw (1.74) - Outcome: LOST - Profit: -100
Date: 27. Dec 08:30 - Sport: soccer - Teams: Canberra (w) vs Brisbane Roar (w) - Score: 1 - 1 - Prediction: Home or Draw (2.02) - Outcome: WIN - Profit: 102
Date: 26. Dec 16:30 - Sport: soccer - Teams: Virtus Francavilla vs Juve Stabia - Score: 1 - 1 - Prediction: Under 2,5 goals (1.7) - Outcome: WIN - Profit: 70
Date: 26. Dec 15:00 - Sport: soccer - Teams: Bamber Bridge vs Marine - Score: 1 - 2 - Prediction: Both to Score - YES (1.63) - Outcome: WIN - Profit: 62
Date: 26. Dec 13:00 - Sport: soccer - Teams: Exeter City vs Yeovil Town - Score: 2 - 1 - Prediction: Under 2,5 goals (1.79) - Outcome: LOST - Profit: -100
Date: 25. Dec 17:00 - Sport: soccer - Teams: Hapoel Rishon LeZion vs Hapoel Ramat Gan - Score: 1 - 1 - Prediction: Draw or Away (1.6) - Outcome: WIN - Profit: 60
Date: 24. Dec 17:00 - Sport: soccer - Teams: Antalyaspor vs Fenerbahce - Score: 0 - 0 - Prediction: Away win (1.87) - Outcome: LOST - Profit: -100
Date: 23. Dec 13:30 - Sport: soccer - Teams: Pistoiese vs Arzachena - Score: 2 - 0 - Prediction: Both to score - NO (1.83) - Outcome: WIN - Profit: 83
Date: 23. Dec 13:00 - Sport: soccer - Teams: Volos NFC vs Kerkyra - Score: 2 - 1 - Prediction: Home win (1.8) - Outcome: WIN - Profit: 80
Date: 22. Dec 15:00 - Sport: soccer - Teams: Nuneaton Town vs Guiseley AFC - Score: 1 - 3 - Prediction: Both to Score - YES (1.75) - Outcome: WIN - Profit: 75
